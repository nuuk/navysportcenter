<?php
require "../utils/db.php";

if(!has_permission("DELETE_ATTENDANCE")){
    respond("Permission denied", 403);
}

$conn = connectDB();

if (!$conn) {
    echo "An error occurred.\n";
    exit;
}


if (!empty($_POST['date']) & !empty($_POST['client'])) {
    $fecha = $_POST['date'];
    $cliente = $_POST['client'];

    $sql = "DELETE FROM asistencia |
    WHERE fecha = '{$fecha}' and cliente = {$cliente}";

    $result = pg_query($conn, $sql);

    if (!$result) {
        echo "An error occurred.\n";
        exit;
    }

}
closeDB($conn);
?>
