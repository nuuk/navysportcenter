<?php
  require "../utils/db.php";
  require "../utils/forms.php";
  require "../utils/http.php";
  require "../utils/auth.php";

if(!has_permission("ADD_CLIENT")){
    respond("Permission denied", 403);
}

  $form_messages = new stdClass();
  $form_messages->errors = new stdClass();

  //FIRST_NAME VALIDATION
  $first_name = validate_POST_field(
    "first_name",
    "/^[a-zA-ZÀ-ÿ ]*$/",
    $form_messages->errors,
    true
  );
  //LAST_NAME VALIATION
  $last_name = validate_POST_field(
    "last_name",
    "/^[a-zA-ZÀ-ÿ ]*$/",
    $form_messages->errors,
    true
  );

  // EMAIL VALIDATION
  $email = validate_POST_field(
    "email", 
    "/^[a-z0-9!#$%&'*+\/\=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/\=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/", 
    $form_messages->errors,
    true
  );

  // PHONE VALIDATION
  $phone = clean_phone(validate_POST_field(
    "phone", 
    "/(^$|^(\+?[0-9]{1,3})?[\s-]?(\(?[0-9]{2,4}\)?)[\s-]?([0-9]{3,4})[\s-]?([0-9]{4})$)/", 
    $form_messages->errors
  ));

  if(count(get_object_vars($form_messages->errors)) > 0){
    respond($form_messages, 400);
  }

  $conn = connectDB();
  $query = "INSERT INTO Usuario (nombre, apellido, email, telefono, tipo) VALUES ('".$first_name."','".$last_name."','".$email."','".$phone."', 1)";
  if(!pg_connection_busy($conn)){
    if(pg_send_query($conn, $query)){
      $res = pg_get_result($conn);
      if($res){
        $err = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
        if($err){
          if($err == PGSQL_UNIQUE_VIOLATION){
            respond('Email already in use', 400);
          }
        }
      }
    }
  }
  closeDB($conn);

  respond("Client created successfully", 201);

?>
