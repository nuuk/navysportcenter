<?php
require "../utils/db.php";
require "../utils/forms.php";
require "../utils/http.php";
require "../utils/auth.php";

if(!has_permission("ADD_MEMBERSHIP")){
    respond("Permission denied", 403);
}

$conn = connectDB();

if (!$conn) {
    echo "Error";
    exit;
}

$form_messages = new stdClass();
$form_messages->errors = new stdClass();

$sql = "";

$nom = validate_POST_field('nombre', "/^[a-zA-ZÀ-ÿ ]*$/", $form_messages->errors, true);
$descr = validate_POST_field('descrip', "/^[a-zA-ZÀ-ÿ ]*$/", $form_messages->errors, true);
$prec = validate_POST_field('precio', "/[0-9]/", $form_messages->errors, true);
$serv = validate_POST_field('servicios', "/[.\s]*/", $form_messages->errors, true);

if(count(get_object_vars($form_messages->errors)) > 0){
    respond($form_messages, 400);
}

//$sql = "SELECT FROM add_membership('{$nom}', '{$descr}', '{$prec}')";
$sql = "INSERT INTO membresia(nombre, descripcion, precio) VALUES ('{$nom}', '{$descr}', '{$prec}')
        RETURNING id;";

//$result = pg_query($conn, $sql);
if(pg_send_query($conn, $sql)) {
    $result = pg_get_result($conn);
    $id_m = pg_fetch_result($result, 0);
} else {
    respond("Something went wrong with query", 400);
}

if (!$result) {
    echo "Error";
    exit;
}

// add services
$dec_serv = json_decode($serv, true);
if (is_array($dec_serv)) {
    $DataArr = array();
    foreach($dec_serv as $sv) {
        $id_s = pg_escape_string($sv[0]);
        $cant = pg_escape_string($sv[2]);

        $DataArr[] = "($id_m, $id_s, $cant)";
    }

    $sql = "INSERT INTO membresia_servicio(id_membresia, id_servicio, cantidad)
            VALUES ";
    $sql .= implode(',', $DataArr);

    $result = pg_query($conn, $sql);
    if (!$result) {
        echo "Error";
        exit;
    }

}
echo $result;

closeDB($conn);

?>
