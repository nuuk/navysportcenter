<?php
  require "../utils/db.php";
  require "../utils/forms.php";
  require "../utils/http.php";
  require "../utils/auth.php";

if(!has_permission("GET_EMPLOYEES")){
  respond("Permission denied", 403);
}

  $form_messages= new stdClass();
  $form_messages->errors = new stdClass();

  $id = validate_GET_field(
    "id",
    "/^[0-9]+$/",
    $form_messages->errors
  );

  if(count(get_object_vars($form_messages->errors)) > 0){
    respond($form_messages, 400);
  }

  $conn = connectDB();

  $query = "SELECT * FROM api_get_users(FALSE)";
  if(strlen($id) > 0){
    $query = "SELECT * FROM api_get_user($id, FALSE)";
  }

  $clients = array();

  if(!pg_connection_busy()){
    if(pg_send_query($conn, $query)){
      $res = pg_get_result($conn);
      if(pg_num_rows($res) > 0){
        while($row = pg_fetch_assoc($res)){
          $clients[] = $row;
        }
      } else {
        respond("Employee not found.", 404);
      }
    }
  }

  closeDB($conn);

  respond("Employees retrieved successfully", 200, $clients);
?>
