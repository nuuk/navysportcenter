<?php
require "../utils/db.php";
require "../utils/forms.php";
require "../utils/http.php";
require "../utils/auth.php";

if(!is_valid_token()){
    respond("Not authenticated", 401);
} else if(!has_permission("ADD_MEMBERSHIP")){
    respond("Permission denied", 403);
}

$conn = connectDB();

if (!$conn) {
    echo "A conn error occurred.\n";
    exit;
}

$sql = "SELECT id as service_id, nombre as servicio, 0 as cantidad 
        FROM servicio;";

$result = pg_query($conn, $sql);

if (!$result) {
    echo "An unexpected error occurred.\n";
    exit;
}

$arr = pg_fetch_all($result);

echo json_encode($arr);

closeDB($conn);

?>
