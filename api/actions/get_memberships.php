<?php
require "../utils/db.php";
require "../utils/forms.php";
require "../utils/http.php";
require "../utils/auth.php";

if(!has_permission("GET_MEMBERSHIPS")){
    respond("Permission denied", 403);
}

$conn = connectDB();

if (!$conn) {
    echo "An error occurred";
    exit;
}

$sql = "SELECT id, nombre, precio, descripcion FROM membresia;";

$result = pg_query($conn, $sql);

if (!$result) {
  respond("Something went wrong", 500);
}

$arr = pg_fetch_all($result);

respond(
  "",
  200,
  $arr
);

closeDB($conn);

?>
