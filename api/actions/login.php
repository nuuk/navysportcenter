<?php
  require "../utils/http.php";
  require "../utils/forms.php";
  require "../utils/db.php";

  $form_messages = new stdClass();
  $form_messages->errors = new stdClass();

  $email = validate_POST_field(
    "email",
    "/^[a-z0-9!#$%&'*+\/\=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/\=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/", 
    $form_messages->errors,
    true
  );

  $password = validate_POST_field(
    "password",
    "/^.*$/",
    $form_messages->errors,
    true
  );

  if(count(get_object_vars($form_messages->errors)) > 0){
    respond($form_messages, 400);
  }

  $password = hash("sha256", $password, false);

  $db = connectDB();

  if(!pg_connection_busy($db)){
    $query = "SELECT * FROM login('$email', '$password');";
    $req = pg_send_query($db, $query);
    if($req){
      $res = pg_get_result($db);
      if($res){
        $row = pg_fetch_assoc($res);
        if(strlen($row["login"]) > 0){
          $data = new stdClass();
          $data->auth_token = $row["login"];
          closeDB($db);
          respond("Successful login", 200, $data);
        } else {
          closeDB($db);
          respond("Invalid email/password", 401);
        }
      }
    }
  }

  closeDB($db);
?>
