<?php
  require "../utils/db.php";
  require "../utils/forms.php";
  require "../utils/http.php";
  require "../utils/auth.php";

if(!has_permission("DELETE_CLIENT")){
    respond("Permission denied", 403);
  }

  $field_errors = new stdClass();
  $field_errors->errors = new stdClass();

  $id = validate_GET_field(
    "id",
    "/^[0-9]+$/",
    $field_errors->errors,
    true
  );

  if(count(get_object_vars($field_errors->errors)) > 0){
    respond("Invalid client ID", 400);
  }

  $conn = connectDB();

  $query = "DELETE FROM Usuario WHERE id = ".$id.";";

  if(!pg_connection_busy()){
    if(pg_send_query($conn, $query)){
      $res = pg_get_result($conn);
      if($res){
        if(pg_affected_rows($res) < 1){
          closeDB($conn);
          respond("Client not found", 404);
        } 
      }
    }
  }

  closeDB($conn);
  respond("Client deleted successfully", 200);

?>

