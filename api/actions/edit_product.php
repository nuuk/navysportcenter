<?php
require "../utils/db.php";
require "../utils/forms.php";
require "../utils/http.php";
require "../utils/auth.php";

if(!has_permission("EDIT_PRODUCT")){
    respond("Permission denied", 403);
}

$form_messages = new stdClass();
$form_messages->errors = new stdClass();

//PRODUCT ID VALIDATION
validate_POST_field(
    "id_product",
    "/^[0-9]\d*$/",
    $form_messages->errors,
    true
);

//NAME VALIDATION
validate_POST_field(
    "name",
    "/^(?!\s*$).+/",
    $form_messages->errors,
    true
);

//PRICE VALIDATION
validate_POST_field(
    "price",
    "/^[+]?([.]\d+|\d+[.]?\d*)$/",
    $form_messages->errors,
    true
);

//QUANTITY VALIDATION
validate_POST_field(
    "quantity",
    "/^[0-9]\d*$/",
    $form_messages->errors,
    true
);

//TYPE VALIDATION
validate_POST_field(
    "type",
    "/^[0-9]\d*$/",
    $form_messages->errors,
    true
);

//SIZE VALIDATION
validate_POST_field(
    "size",
    "/^(XS|S|M|L|XL|U|NA)\d*$/",
    $form_messages->errors,
    true
);

//SEX VALIDATION
validate_POST_field(
    "sex",
    "/(H|M|U|NA)/",
    $form_messages->errors,
    true
);

//DESCRIPTION VALIDATION
validate_POST_field(
    "description",
    "/^(?!\s*$).+/",
    $form_messages->errors,
    true
);

if(count(get_object_vars($form_messages->errors)) > 0){
    respond($form_messages, 400);
}

$conn = connectDB();

if (!$conn) {
    respond("Internal Server Error", 500);
}

$id_producto = $_POST['id_product'];
$nombre = $_POST['name'];
$precio = $_POST['price'];
$cantidad = $_POST['quantity'];
$tipo = $_POST['type'];
$talla = $_POST['size'];
$sexo = $_POST['sex'];
$descripcion = $_POST['description'];
$file_name = NULL;

if (isset($_POST['image_name'])) {
    $file_name = $_POST['image_name'];
}

if ($cantidad > 0) {
    $estado = 1;
} else {
    $estado = 0;
}

if ( $_FILES['image'] ) {

    if (is_null($file_name)) {
        $name = $_FILES['image']['name'];
        $random_prefix = rand(100000,999999);
        $file_name = $random_prefix . '.' . strtolower(pathinfo($name, PATHINFO_EXTENSION));    
    }
    
    $target_dir = '../../src/assets/img/products/';
    $target_file = $target_dir . $file_name;
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($file_name, PATHINFO_EXTENSION));
    
    // Check if image file is an actual image or fake image
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if($check == false) {
        respond("File is not an image.", 400);
        $uploadOk = 0;
    }

    // Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
    && $imageFileType != "gif" ) {
        respond("Sorry, only JPG, JPEG, PNG & GIF files are allowed.", 400);
        $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        respond("Sorry, your file was not uploaded.", 500);
    // if everything is ok, try to upload file
    } else {
        if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
            respond("Sorry, there was an error uploading your file.", 500);
        }
    }
}

$sql = "
    UPDATE producto
    SET 
        nombre = '{$nombre}',
        precio = {$precio},
        cantidad = {$cantidad},
        estado = {$estado},
        tipo = {$tipo},
        talla = '{$talla}',
        sexo = '{$sexo}',
        descripcion = '{$descripcion}',
        foto = '{$file_name}'
    WHERE id = $id_producto;
    ";

pg_query($conn, $sql);

closeDB($conn);

respond("Product updated successfully", 201);
?>
