<?php
require "../utils/db.php";
require "../utils/forms.php";
require "../utils/http.php";
require "../utils/auth.php";

if(!has_permission("REGISTER_ATTENDANCE")){
    respond("Permission denied", 403);
}

$form_messages = new stdClass();
$form_messages->errors = new stdClass();

//DATE VALIDATION
validate_POST_field(
    "date",
    "/^(19|20)\d\d(-)(0[1-9]|1[012])(-)(0[1-9]|[12][0-9]|3[01])$/",
    $form_messages->errors,
    true
);

//TIME VALIDATION
validate_POST_field(
    "time",
    "/^([0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/",
    $form_messages->errors,
    true
);

if(count(get_object_vars($form_messages->errors)) > 0){
    respond($form_messages, 400);
}

$conn = connectDB();

if (!$conn) {
    respond("Internal Server Error", 500);
}

$fecha = $_POST['date'];
$tiempo = $_POST['time'];
$clientes_json = $_POST['clients'];

$clientes = json_decode($clientes_json, TRUE);

if(is_array($clientes)) {

    $DataArr = array();
    foreach($clientes as $row){
        $cliente = pg_escape_string($row['id']);
        $estado = pg_escape_string($row['estado']);

        $DataArr[] = "('$fecha', '$tiempo', $cliente, $estado)";
    }

    $sql = "INSERT INTO asistencia (fecha, tiempo, cliente, estado) VALUES ";
    $sql .= implode(',', $DataArr);

    $result = pg_query($conn, $sql);

} else {
    respond("Bad Request", 400);
}

closeDB($conn);

respond("Attendance created successfully", 201);
?>
