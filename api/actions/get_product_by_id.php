<?php
require "../utils/db.php";
require "../utils/forms.php";
require "../utils/http.php";
require "../utils/auth.php";

if(!has_permission("GET_PRODUCTS")){
    respond("Permission denied", 403);
}

$form_messages = new stdClass();
$form_messages->errors = new stdClass();

//PRODUCT ID VALIDATION
validate_POST_field(
    "id_product",
    "/^[0-9]\d*$/",
    $form_messages->errors,
    true
);

if(count(get_object_vars($form_messages->errors)) > 0){
    respond($form_messages, 400);
}

$conn = connectDB();

if (!$conn) {
    respond("Internal Server Error", 500);
}

$id_producto = $_POST['id_product'];

$sql = "
    SELECT * 
    FROM producto
    WHERE id = $id_producto;
";

$result = pg_query($conn, $sql);

if (!$result) {
    respond("Internal Server Error", 500);
    exit;
}

$arr = pg_fetch_all($result);

closeDB($conn);

echo json_encode($arr);

?>
