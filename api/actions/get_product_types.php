<?php
require "../utils/db.php";
require "../utils/forms.php";
require "../utils/http.php";
require "../utils/auth.php";

if(!has_permission("GET_PRODUCT_TYPES")){
    respond("Permission denied", 403);
}

$conn = connectDB();

if (!$conn) {
    respond("Internal Server Error", 500);
}

$sql = "SELECT * FROM tipoproducto";

$result = pg_query($conn, $sql);

if (!$result) {
    respond("Internal Server Error", 500);
    exit;
}

$arr = pg_fetch_all($result);

closeDB($conn);

echo json_encode($arr);

?>
