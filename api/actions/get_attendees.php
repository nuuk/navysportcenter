<?php
require "../utils/db.php";
require "../utils/forms.php";
require "../utils/http.php";

$form_messages = new stdClass();
$form_messages->errors = new stdClass();

//DATE VALIDATION
validate_POST_field(
    "date",
    "/^(19|20)\d\d(-)(0[1-9]|1[012])(-)(0[1-9]|[12][0-9]|3[01])$/",
    $form_messages->errors,
    true
);

//TIME VALIDATION
validate_POST_field(
    "time",
    "/^([0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/",
    $form_messages->errors,
    true
);

if(count(get_object_vars($form_messages->errors)) > 0){
    respond($form_messages, 400);
}

$conn = connectDB();

if (!$conn) {
    respond("Internal Server Error", 500);
}

$fecha = $_POST['date'];
$tiempo = $_POST['time'];

$sql = "SELECT * FROM get_attendees('{$fecha}', '{$tiempo}')";

$result = pg_query($conn, $sql);

if (!$result) {
    echo "An error occurred.\n";
    exit;
}

$arr = pg_fetch_all($result);

echo json_encode($arr);

closeDB($conn);

?>