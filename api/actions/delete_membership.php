<?php
require "../utils/db.php";
require "../utils/forms.php";
require "../utils/http.php";
require "../utils/auth.php";

if(!has_permission("DELETE_MEMBERSHIP")){
  respond("Permission denied", 403);
}

$conn = connectDB();

if (!$conn) {
    echo "Connection error";
    exit;
}

$form_messages = new stdClass();
$form_messages->errors = new stdClass();

$sql = "";

$id_membership = validate_POST_field('id', "/[0-9]/", $form_messages->errors, true);

if(count(get_object_vars($form_messages->errors)) > 0){
    respond($form_messages, 400);
}

$sql = "DELETE FROM membresia WHERE id='{$id_membership}';";
$result = pg_query($conn, $sql);

if (!$result) {
    echo "Error";
    exit;
}

$sql = "DELETE FROM membresia_servicio WHERE id_membresia='{$id_membership}';";
$result = pg_query($conn, $sql);

if (!$result) {
    echo "Error";
    exit;
}

echo $result;

closeDB($conn);

?>
