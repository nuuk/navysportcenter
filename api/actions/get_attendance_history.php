<?php
require "../utils/db.php";
require "../utils/forms.php";
require "../utils/http.php";
require "../utils/auth.php";

if(!has_permission("GET_ATTENDANCES")){
    respond("Permission denied", 403);
}

$form_messages = new stdClass();
$form_messages->errors = new stdClass();

//LOWER DATE VALIDATION
validate_POST_field(
    "lower_date",
    "/^(19|20)\d\d(-)(0[1-9]|1[012])(-)(0[1-9]|[12][0-9]|3[01])$/",
    $form_messages->errors,
    true
);

//UPPER DATE VALIDATION
validate_POST_field(
    "upper_date",
    "/^(19|20)\d\d(-)(0[1-9]|1[012])(-)(0[1-9]|[12][0-9]|3[01])$/",
    $form_messages->errors,
    true
);

//LOWER TIME VALIDATION
validate_POST_field(
    "lower_time",
    "/^([0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/",
    $form_messages->errors,
    true
);

//UPPER TIME VALIDATION
validate_POST_field(
    "upper_time",
    "/^([0-9]|1[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/",
    $form_messages->errors,
    true
);

if(count(get_object_vars($form_messages->errors)) > 0){
    respond($form_messages, 400);
}

$conn = connectDB();

if (!$conn) {
    respond("Internal Server Error", 500);
}

$fecha_menor = $_POST['lower_date'];
$fecha_mayor = $_POST['upper_date'];
$tiempo_menor = $_POST['lower_time'];
$tiempo_mayor = $_POST['upper_time'];

$sql = "
SELECT * FROM get_attendance_history(
    DATE '{$fecha_menor}',
    DATE '{$fecha_mayor}',
    TIME '{$tiempo_menor}',
    TIME '{$tiempo_mayor}'
)";

$result = pg_query($conn, $sql);

if (!$result) {
    echo "An error occurred.\n";
    exit;
}

$arr = pg_fetch_all($result);

echo json_encode($arr);

closeDB($conn);

?>
