<?php
  require "../utils/db.php";
  require "../utils/forms.php";
  require "../utils/http.php";
  require "../utils/auth.php";

  $form_messages = new stdClass();
  $form_messages->errors = new stdClass();

  if(!is_valid_token()){
    respond("Not authenticated", 401);
  } else if(!has_permission("EDIT_EMPLOYEE")){
    respond("Permission denied", 403);
  }

  $email = validate_POST_field(
    "email",
    "/^[a-z0-9!#$%&'*+\/\=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/\=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/",
    $form_messages->errors,
  );

  $phone = clean_phone(validate_POST_field(
    "phone",
    "/(^$|^(\+?[0-9]{1,3})?[\s-]?(\(?[0-9]{2,4}\)?)[\s-]?([0-9]{3,4})[\s-]?([0-9]{4})$)/", 
    $form_messages->errors
  ));

  $first_name = validate_POST_field(
    "first_name",
    "/^[a-zA-ZÀ-ÿ ]*$/",
    $form_messages->errors
  );

  $last_name = validate_POST_field(
    "last_name",
    "/^[a-zA-ZÀ-ÿ ]*$/",
    $form_messages->errors
  );

  $password = validate_POST_field(
    "password",
    "/^.{8,}$/",
    $form_messages->errors,
  );

  $id = validate_GET_field(
    "id",
    "/[0-9]+/",
    $form_messages->errors,
    true
  );

  if(count(get_object_vars($form_messages->errors)) > 0){
    respond($form_messages, 400);
  }

  $empty_form = true;
  $fields = ["email", "first_name", "last_name", "phone", "password"];
  foreach($fields as &$field){
    $empty_form&= !isset($_POST[$field]);
  }

  $db = connectDB();

  if(!pg_connection_busy($db)){
    $query = "SELECT * FROM Usuario WHERE id = $id;";
    $req = pg_send_query($db, $query);
    if($req){
      $res = pg_get_result($db);
      if($res){
        if(pg_fetch_assoc($res)["tipo"] == 0){
          respond("Employee not found.", 404);
        }
      } else {
        respond("Employee not found.", 404);
      }
    }
  }

  if(!$empty_form && !pg_connection_busy($db)){
    $query = "UPDATE Usuario SET ";
    $query .= "email = '$email'";
    $query .= ", nombre = '$first_name'";
    $query .= ", apellido = '$last_name'";
    $query .= ", telefono = '$phone'";
    $query .= ", contrasena = '$password'";
    $query .= " WHERE id = $id AND tipo = 0";

    $req = pg_send_query($db, $query);
    if($req){
      $res = pg_get_result($db);
      if($res){
        $err = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
        if($err){
          if($err == PGSQL_UNIQUE_VIOLATION){
            respond('Email already in use', 400);
          }
        }
        if(pg_affected_rows($res) > 0){
          respond("Employee updated successfully.", 200);
        }
      }
    }
  }
  closeDB($db);
  respond("No changes detected", 200);
