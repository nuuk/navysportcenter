<?php
//session_start();
function get_permissions(){
  /*
  if(isset($_SESSION["permissions"])){
    return $_SESSION["permissions"];
  }
   */
  $form_messages = new stdClass();
  $form_messages->errors = new stdClass();

  $token = validate_POST_field(
    "auth_token",
    "/^[a-zA-Z0-9]{64}$/", 
    $form_messages->errors,
  );
  
  if(!isset($_POST["auth_token"])){
    respond("Not authenticated", 401);
  }

  if(count(get_object_vars($form_messages->errors)) > 0){
    respond($form_messages, 400);
  }

  $db = connectDB();

  $permissions = array();

  if(!pg_connection_busy()){
    $query = "SELECT * FROM validate_token('$token');";
    $req = pg_send_query($db, $query);
    if($req){
      $res = pg_get_result($db);
      if($res){
        if(pg_num_rows($res) > 0){
          while($row = pg_fetch_row($res)){
            $permissions[$row[0]] = true;
          }
        } else {
          closeDB($db);
          respond("Not authenticated", 401, "/login");
        }
      }
    }
  }
  closeDB($db);
  //$_SESSION["permissions"] = $permissions;
  return $permissions;
}
function is_valid_token(){
  return count(get_permissions()) > 0;
}
function has_permission($slug){
  return isset(get_permissions()[$slug]);
}
?>
