<?php
require "../settings.php";

define("PGSQL_UNIQUE_VIOLATION", 23505);

function connectDB(){
  // PRODUCTION CREDENTIALS
  $host = "";
  $port = 5432;
  $dbname = "";
  $user = "";
  $db_password = getenv("NSC_DB_PWD");

  // DEVELOPMENT CREDENTIALS
  if(DEVELOPMENT){
    $host = "ec2-54-83-33-14.compute-1.amazonaws.com";
    $port = 5432;
    $dbname = "d65ctciesds13s";
    $user = "vdirbrhpwqyjgg";
    $db_password = getenv("NSC_DEVDB_PWD");
  }

  if(LOCAL){
    $host = getenv("NSC_LCDB_HOST");
    $port = getenv("NSC_LCDB_PORT");
    $dbname = getenv("NSC_LCDB_DB");
    $user = getenv("NSC_LCDB_USER");
    $db_password = getenv("NSC_LCDB_PWD");
  }

  return pg_connect("host=$host port=$port dbname=$dbname user=$user password=$db_password");
}

function closeDB($conn){
  return pg_close($conn);
}
?>
