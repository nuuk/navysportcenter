<?php
  function respond($message, $status_code, $data = False){
    $response = new stdClass();
    http_response_code($status_code);
    $response->message = $message;
    if($data){
      $response->data = $data;
    }
    exit(json_encode($response));
  }
?>
