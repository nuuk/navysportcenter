<?php
  function validate_POST_field($field_name, $regex, &$target, $required = false){
    if(isset($_POST[$field_name])){
      if(!preg_match($regex, $_POST[$field_name])){
        $target->$field_name = "Invalid $field_name";
      }
    } else {
      if($required){
        $target->$field_name = "The $field_name is required";
      }
    }
    if(isset($_POST[$field_name])){
      return $_POST[$field_name];
    }
    return "";
  }

  function validate_GET_field($field_name, $regex, &$target, $required = false){
    if(isset($_GET[$field_name])){
      if(!preg_match($regex, $_GET[$field_name])){
        $target->$field_name = "Invalid $field_name";
      }
    } else {
      if($required){
        $target->$field_name = "The $field_name is required";
      }
    }
    if(isset($_GET[$field_name])){
      return $_GET[$field_name];
    }
    return "";
  }

  function clean_phone($phone){
    return preg_replace("/[\+\(\)\-\s]/", "", $phone);
  }
?>
