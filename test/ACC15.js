const { assert } = require("chai");
const conn = require("./utils/db.js").conn();
const axios = require("axios");
axios.defaults.baseURL = "http://localhost:8080";
const dbadduser = require("./utils/db.js").adduser;
const shajs = require("sha.js");
const qs = require("qs");

suite("ACC15 - Iniciar sesión como empleado", function(){
  var data, user_id;
  var url = "/api/actions/login.php";
  var email = "jcoltrane@sprout.bio";
  var password = "b3l1k3w4t3r!";
  var hashed_password = shajs('sha256').update(password).digest('hex');

  suiteSetup(done => {
    conn.connect()
      .then(() => {
        return dbadduser(
          conn, {
              nombre: "John",
              apellido: "Coltrane",
              email: email,
              tipo: 0,
              contrasena: hashed_password
            }, [ 
              "GET_CLIENTS"
            ]
        );
      })
      .then(dbres => {
        user_id = dbres.rows[0].id_usuario;
        done();
      })
      .catch(err => done(err));
  });

  setup(done => {
    data = {};
    done();
  });

  test("user enters invalid email returns 400", done => {
    data["email"] = "klktudice";
    data["password"] = "wawawa";
    axios.post(url, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeeded with status code ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.status, 400);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user enters invalid email returns correct message", done => {
    data["email"] = "klktudice";
    data["password"] = "wawawa";
    axios.post(url, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeeded with status code ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.data.message.errors.email, 'Invalid email');
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
    });

  required_fields = [
    {name: "email"}, {name: "password"}
  ];
  required_fields.forEach(field => {
    test(`user doesn't enter a ${field.name} returns 400`, done => {
      axios.post(url, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeeded with status code ${response.status}`);
          done();
        })
        .catch(axiosError => {
          if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.status, 400);
            done();
          } else {
            done(axiosError);
          }
        })
        .catch(err => done(err));
    });
    test(`user doesn't enter a ${field.name} returns correct message`, done => {
      axios.post(url, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeeded with status code ${response.status}`);
          done();
        })
        .catch(axiosError => {
          if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.data.message.errors[field.name], `The ${field.name} is required`);
            done();
          } else {
            done(axiosError);
          }
        })
        .catch(err => done(err));
    });
  });

  test("user enters SQLINJECTION returns 400", done => {
    data["email"] = `'jkj'; DELETE FROM Usuario WHERE id = ${user_id}; SELECT FROM usuario WHERE password = '`;
    axios.post(url, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeeded with status code ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.status, 400);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user enters SQLINJECTION returns correct message", done => {
    data["email"] = `'jkj'; DELETE FROM Usuario WHERE id = ${user_id}; SELECT FROM usuario WHERE password = '`;
    data["password"] = "ye";
    axios.post(url, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeeded with status code ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.data.message.errors.email, `Invalid email`);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user enters SQLINJECTION doesn't affect db", done => {
    data["email"] = `'jkj'; DELETE FROM Usuario WHERE id = ${user_id}; SELECT FROM usuario WHERE password = '`;
    data["password"] = "ye";
    axios.post(url, qs.stringify(data))
      .catch(err => {
        if(!err.response){
          done(err);
        }
      })
      .finally(() => {
        conn.query(`SELECT FROM Usuario WHERE id = ${user_id}`)
          .then(dbres => {
            assert.lengthOf(dbres.rows, 1);
            done();
          })
          .catch(err => done(err));
      });
  });
  test("user doesn't exist returns 401", done => {
    data["email"] = "patypatpat@gmail.com";
    data["password"] = "beb1d4";
    axios.post(url, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeeded with status code ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.status, 401);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user doesn't exist returns correct message", done => {
    data["email"] = "patypatpat@gmail.com";
    data["password"] = password;
    axios.post(url, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeeded with status code ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.data.message, "Invalid email/password");
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user enters wrong password returns 401", done => {
    data["email"] = email;
    data["password"] = "da";
    axios.post(url, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeeded with status code ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.status, 401);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user enters wrong password returns correct message", done => {
    data["email"] = email;
    data["password"] = "da";
    axios.post(url, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeeded with status code ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.data.message, "Invalid email/password");
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user successful login returns 200", done => {
    data["email"] = email;
    data["password"] = password;
    axios.post(url, qs.stringify(data))
      .then(response => {
        assert.equal(response.status, 200);
        done();
      })
      .catch(err => done(err));
  });
  test("user successful login returns correct message", done => {
    data["email"] = email;
    data["password"] = password;
    axios.post(url, qs.stringify(data))
      .then(response => {
        assert.equal(response.data.message, "Successful login");
        done();
      })
      .catch(err => done(err));
  });
  test("user successful login returns a valid token", done => {
    data["email"] = email;
    data["password"] = password;
    axios.post(url, qs.stringify(data))
      .then(response => {
        assert.lengthOf(response.data.data["auth_token"], 64);
        done();
      })
      .catch(err => done(err));
  });
  test("user successful login returns a token present in db", done => {
    let token = "";
    data["email"] = email;
    data["password"] = password;
    axios.post(url, qs.stringify(data))
      .then(response => {
        token = response.data.data["auth_token"];
        return conn.query(`
          SELECT * FROM Token WHERE token = '${token}';
        `);
      })
      .then(dbres => {
        assert.lengthOf(dbres.rows, 1);
        done();
      })
      .catch(err => done(err));
  });
  test("user successful login returns same valid token if it hasn't expired", done => {
    let init_token = "";
    data["email"] = email;
    data["password"] = password;
    axios.post(url, qs.stringify(data))
      .then(res => {
        init_token = res.data["data"].auth_token;
        return axios.post(url, qs.stringify(data));
      })
      .then(response => {
        assert.equal(init_token, response.data.data["auth_token"]);
        done();
      })
      .catch(err => done(err));
  });

  test("user successful login returns different valid token if it expired", done => {
    let init_token = "";
    data["email"] = email;
    data["password"] = password;
    axios.post(url, qs.stringify(data))
      .then(res => {
        init_token = res.data["data"].auth_token;
        return conn.query(`
          UPDATE Token SET expiry = to_timestamp('21 Jan 1999', 'DD Mon YYYY')
          WHERE token = '${init_token}';
        `);
      })
      .then(dbres => {
        return axios.post(url, qs.stringify(data));
      })
      .then(response => {
        assert.notEqual(init_token, response.data.data["auth_token"]);
        done();
      })
      .catch(err => done(err));
  });
  test("user doesn't send token returns 401", done => {
    axios.post("/api/actions/verify_token.php", qs.stringify({}))
      .then(response => {
        assert.fail(`Request succeeded with status ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.status, 401);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user doesn't send token returns correct message", done => {
    axios.post("/api/actions/verify_token.php", qs.stringify({}))
      .then(response => {
        assert.fail(`Request succeeded with status ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.data.message, "Not authenticated");
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user uses expired token returns 401", done => {
    let token = "";
    data["email"] = email;
    data["password"] = password;
    axios.post(url, qs.stringify(data))
      .then(res => {
        token = res.data["data"].auth_token;
        return conn.query(`
          UPDATE Token SET expiry = to_timestamp('21 Jan 1999', 'DD Mon YYYY')
          WHERE token = '${token}';
        `);
      })
      .then(dbres => {
        return axios.post("/api/actions/verify_token.php", qs.stringify({
          auth_token: token
        }));
      })
      .then(response => {
        assert.fail(`Request succeeded with status ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.status, 401);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user uses expired token returns correct message", done => {
    let token = "";
    data["email"] = email;
    data["password"] = password;
    axios.post(url, qs.stringify(data))
      .then(res => {
        token = res.data["data"].auth_token;
        return conn.query(`
          UPDATE Token SET expiry = to_timestamp('21 Jan 1999', 'DD Mon YYYY')
          WHERE token = '${token}';
        `);
      })
      .then(dbres => {
        return axios.post("/api/actions/verify_token.php", qs.stringify({
          auth_token: token
        }));
      })
      .then(response => {
        console.debug(response.data);
        assert.fail(`Request succeeded with status ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.data.message, "Not authenticated");
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user uses SQLINJECTION as token returns 400", done => {
    let token = `'; DELETE FROM Usuario WHERE id = ${user_id}; SELECT FROM Token WHERE token = '`;
    axios.post("/api/actions/verify_token.php", qs.stringify({
          auth_token: token
      }))
      .then(response => {
        assert.fail(`Request succeeded with status ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.status, 400);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user uses SQLINJECTION as token returns correct message", done => {
    let token = `'; DELETE FROM Usuario WHERE id = ${user_id}; SELECT FROM Token WHERE token = '`;
    axios.post("/api/actions/verify_token.php", qs.stringify({
          auth_token: token
      }))
      .then(response => {
        assert.fail(`Request succeeded with status ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.data.message.errors.auth_token, "Invalid auth_token");
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });
  test("user uses SQLINJECTION as token doesn't affect db", done => {
    let token = `'; DELETE FROM Usuario WHERE id = ${user_id}; SELECT FROM Token WHERE token = '`;
    axios.post("/api/actions/verify_token.php", qs.stringify({
          auth_token: token
      }))
      .catch(err => {
        if(!err.response){
          done(err);
        }
      })
      .finally(() => {
        conn.query(`SELECT FROM Usuario WHERE id = ${user_id}`)
          .then(dbres => {
            assert.lengthOf(dbres.rows, 1);
            done();
          })
          .catch(err => done(err));
      })
  });
  test("user uses expired token prompts login", done => {
    let token = "";
    data["email"] = email;
    data["password"] = password;
    axios.post(url, qs.stringify(data))
      .then(res => {
        token = res.data["data"].auth_token;
        return conn.query(`
          UPDATE Token SET expiry = to_timestamp('21 Jan 1999', 'DD Mon YYYY')
          WHERE token = '${token}';
        `);
      })
      .then(dbres => {
        return axios.post("/api/actions/verify_token.php", qs.stringify({
          auth_token: token
        }));
      })
      .then(response => {
        assert.fail(`Request succeeded with status ${response.status}`);
        done();
      })
      .catch(axiosError => {
        if(axiosError.response){
          let response = axiosError.response;
          assert.equal(response.data.data, "/login");
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });

  teardown(done => {
    done();
  });

  suiteTeardown(done => {
    conn.query(`
      DELETE FROM Usuario WHERE id = ${user_id};
      DELETE FROM Rol WHERE nombre = 'test_role';
    `)
      .then(() => {
        return conn.end();
      })
      .then(() => done())
      .catch(err => done(err));
  });
});
