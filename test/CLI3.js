/* CLI3 - Eliminar cliente */
const { assert } = require("chai");
const conn = require("./utils/db.js").conn();
const dbadduser = require("./utils/db.js").adduser;
const axios = require("axios");
axios.defaults.baseURL = "http://localhost:8080";
const shajs = require("sha.js");
const qs = require("qs");

suite("CLI3 - Eliminar cliente", function(){
  var data, auth_token, alt_auth_token, client_id;

  suiteSetup(function(done){
    conn.connect().then(() => {
      return dbadduser(
        conn, {
            nombre: "Tony",
            apellido: "Montana",
            email: "crazy4coco@scarface.es",
            tipo: 1,
            contrasena: shajs("sha256").update("say_hello").digest("hex")
          }, [ 
            "DELETE_CLIENT"
          ]
      );
    }).then(dbres => {
      client_id = dbres.rows[0].id_usuario;
      return dbadduser(
        conn, {
            nombre: "Miguel",
            apellido: "Montana",
            email: "crazyforcoco@scarface.es",
            tipo: 0,
            contrasena: shajs("sha256").update("say_hello").digest("hex")
          }, [ 
            "DELETE_CLIENT"
          ]
      );
    }).then(dbres => {
      return dbadduser(
        conn, {
            nombre: "Felix",
            apellido: "Koshy",
            email: "pewdieliza@youtube.com",
            tipo: 0,
            contrasena: shajs("sha256").update("say_klk").digest("hex")
        }, [
          "ADD_EMPLOYEE"
        ],
        "alien"
      );
    }).then(() => {
      return axios.post("/api/actions/login.php", qs.stringify({
        email: 'crazyforcoco@scarface.es',
        password: 'say_hello'
      }));
    }).then(response => {
      auth_token = response.data.data.auth_token;
      return axios.post("/api/actions/login.php", qs.stringify({
        email: 'pewdieliza@youtube.com',
        password: 'say_klk'
      }));
    }).then(response => {
        alt_auth_token = response.data.data.auth_token;
        done();
    }).catch(err => {done(err);});
  });

  setup(function(){
    data = {};
  });

  test("user without authentication returns 401", function(done){
    axios.post(`/api/actions/delete_client.php?id=${client_id}`)
      .then(function(response){
        assert.fail(`Request succeded with status code ${response.status}.`);
        done();
      }).catch((axiosErr) => {
        if(axiosErr.response){
          let response = axiosErr.response;
          assert.equal(response.status, 401);
          done();
        } else {
          done(axiosErr);
        }
      }).catch((assertErr) => {
        done(assertErr);
      });
  });

  test("user without authentication returns correct message", function(done){
    axios.post(`/api/actions/delete_client.php?id=${client_id}`)
    .then(function(response){
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    }).catch((axiosErr) => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.data.message, "Not authenticated");
        done();
      } else {
        done(axiosErr);
      }
    }).catch((assertErr) => {
      done(assertErr);
    });
  });
  
  /* M: The user is does not have permissions */
  test("user doesn't have permissions should return 403", function(done){
    data["auth_token"] = alt_auth_token;
    axios.post(`/api/actions/delete_client.php?id=${client_id}`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.status, 403);
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(error => done(error));
  });

  test("user doesn't have permissions should return correct message", function(done){
    data["auth_token"] = alt_auth_token;
    axios.post(`/api/actions/delete_client.php?id=${client_id}`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.data.message, "Permission denied");
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(error => done(error));
  });

  /* M: The client doesnt exist */
  test(`user doesn't enter an existing client returns 404`, function(done){
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/delete_client.php?id=842069690248`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.status, 404);
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(error => done(error));
  });

  test(`user doesn't enter an existing client returns correct message`, function(done){
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/delete_client.php?id=842069690248`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.data.message, `Client not found`);
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(error => done(error));
  });

  test(`user doesn't enter a valid client id returns a 400`, function(done){
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/delete_client.php?id=asdf`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.status, 400);
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(error => done(error));
  });

  test(`user doesn't enter a valid client id returns the correct message`, function(done){
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/delete_client.php?id=asdf`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.data.message, `Invalid client ID`);
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(error => done(error));
  });

  test(`user successfully deletes user returns 200`, function(done){
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/delete_client.php?id=${client_id}`, qs.stringify(data))
    .then(response => {
      return assert.equal(response.status, 200);
    })
    .then(() => {
      return conn.query(`
        INSERT INTO 
        Usuario (nombre, apellido, email, tipo, contrasena)
        VALUES ('Tony', 'Montana', 'crazy4coco@scarface.es', 1, 'say_hello');
      `);
    })
    .then(() => {
      return conn.query(`SELECT * FROM Usuario WHERE email = 'crazy4coco@scarface.es';`);
    })
    .then(dbres => {
      client_id = dbres.rows[0].id;
      done()
    })
    .catch(error => done(error));
  });

  test(`user successfully deletes user returns correct message`, function(done){
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/delete_client.php?id=${client_id}`, qs.stringify(data))
    .then(response => {
      return assert.equal(response.data.message, "Client deleted successfully");
    })
    .then(() => {
      return conn.query(`
        INSERT INTO 
        Usuario (nombre, apellido, email, tipo, contrasena)
        VALUES ('Tony', 'Montana', 'crazy4coco@scarface.es', 1, 'say_hello');
      `);
    })
    .then(() => {
      return conn.query(`SELECT * FROM Usuario WHERE email = 'crazy4coco@scarface.es';`);
    })
    .then(dbres => {
      client_id = dbres.rows[0].id;
      done()
    })
    .catch(error => done(error));
  });

  test(`user successfully deletes user affects db`, function(done){
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/delete_client.php?id=${client_id}`, qs.stringify(data))
    .then(response => {
      return conn.query(`SELECT * FROM Usuario WHERE email = 'crazy4coco@scarface.es';`); 
    })
    .then((dbres) => {
      return assert.lengthOf(dbres.rows, 0);
    })
    .then(() => {
      return conn.query(`
        INSERT INTO 
        Usuario (nombre, apellido, email, tipo, contrasena)
        VALUES ('Tony', 'Montana', 'crazy4coco@scarface.es', 1, 'say_hello');
      `);
    })
    .then(() => {
      return conn.query(`SELECT * FROM Usuario WHERE email = 'crazy4coco@scarface.es';`);
    })
    .then(dbres => {
      client_id = dbres.rows[0].id;
      done()
    })
    .catch(error => done(error));
  });

  test(`user attempts SQL injection, but in returns 400`, function(done){
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/delete_client.php?id='0;SELECT * FROM Usuario'`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.status, 400);
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(error => done(error));
  });


  suiteTeardown(function(done){
    // We delete the test user
    conn.query(`
      DELETE FROM Usuario WHERE email = 'crazy4coco@scarface.es';
      DELETE FROM Usuario WHERE email = 'crazyforcoco@scarface.es';
      DELETE FROM Usuario WHERE email = 'pewdieliza@youtube.com';
      DELETE FROM Rol WHERE nombre = 'test_role';
      DELETE FROM Rol WHERE nombre = 'alien';
    `).then(() => {
      // We close the connection to the database
      return conn.end();
    }).then(() => done()).catch(err => done(err));
  });
});
