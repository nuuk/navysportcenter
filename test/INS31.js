// INS31 - CREAR PLAN DE MEMBRESÍA
const { assert } = require("chai");
const conn = require("./utils/db.js").conn();
const dbadduser = require("./utils/db.js").adduser;
const axios = require("axios");
axios.defaults.baseURL = "http://localhost:8080";
const shajs = require("sha.js");
const qs = require('qs');

suite("INS31 - Crear plan de membresia", function() {
    var data, auth_token;
    suiteSetup(function (done) {
      conn.connect().then(() => {
        return dbadduser(
          conn, {
              nombre: "Tony",
              apellido: "Montana",
              email: "crazy4coco@scarface.es",
              tipo: 0,
              contrasena: shajs("sha256").update("say_hello").digest("hex")
            }, [ 
              "ADD_MEMBERSHIP"
            ]
        );
      }).then(() => {
        return dbadduser(
          conn, {
              nombre: "Felix",
              apellido: "Koshy",
              email: "pewdieliza@youtube.com",
              tipo: 0,
              contrasena: shajs("sha256").update("say_klk").digest("hex")
          }, [
            "ADD_EMPLOYEE"
          ],
          "alien"
        );
      }).then(() => {
        return axios.post("/api/actions/login.php", qs.stringify({
          email: 'crazy4coco@scarface.es',
          password: 'say_hello'
        }));
      }).then(response => {
        auth_token = response.data.data.auth_token;
        return axios.post("/api/actions/login.php", qs.stringify({
          email: 'pewdieliza@youtube.com',
          password: 'say_klk'
        }));
      }).then(response => {
          alt_auth_token = response.data.data.auth_token;
          done();
      }).catch(err => {done(err);});
    });

    setup(function(){
        data = {};
    });

    test("user without authentication returns 401", done => {
        axios.post(`/api/actions/add_membership.php`)
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosErr => {
          if(axiosErr.response){
            let response = axiosErr.response;
            assert.equal(response.status, 401);
            done();
          } else {
            done(axiosErr);
          }
        })
        .catch(err => done(err));
    });

    test("user without authentication returns correct message", done => {
        axios.post(`/api/actions/add_membership.php`)
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosErr => {
          if(axiosErr.response){
            let response = axiosErr.response;
            assert.equal(response.data.message, `Not authenticated`);
            done();
          } else {
            done(axiosErr);
          }
        })
        .catch(err => done(err));
    });

    test("user doesn't have permissions returns 403", done => {
        data["auth_token"] = alt_auth_token;
        axios.post(`/api/actions/add_membership.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosErr => {
          if(axiosErr.response){
            let response = axiosErr.response;
            assert.equal(response.status, 403);
            done();
          } else {
            done(axiosErr);
          }
        })
        .catch(err => done(err));
    });

    test("user doesn't have permissions returns correct message", done => {
        data["auth_token"] = alt_auth_token;
        axios.post(`/api/actions/add_membership.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosErr => {
          if(axiosErr.response){
            let response = axiosErr.response;
            assert.equal(response.data.message, `Permission denied`);
            done();
          } else {
            done(axiosErr);
          }
        })
        .catch(err => done(err));
    });

    test("User attempts to create membership leaving fields blank or wrong chars", done => {
        data["auth_token"] = auth_token;

        //data['nombre'] = "Membresía tdd";
        //data['descrip'] = "Descripción membresía tdd";
        //data['precio'] = 3312;

        axios.post(`/api/actions/add_membership.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosError => {
            if(axiosError.response){
              let response = axiosError.response;
              assert.equal(response.status, 400);
              done();
            } else {
              done(axiosError);
            }
        })
        .catch(err => done(err));
    });

    test("User attempts to create membership leaving fields blank or wrong chars", done => {
        data["auth_token"] = auth_token;

        data['nombre'] = "Membresía tdd";
        //data['descrip'] = "Descripción membresía tdd";
        //data['precio'] = 3312;

        axios.post(`/api/actions/add_membership.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosError => {
            if(axiosError.response){
              let response = axiosError.response;
              assert.equal(response.status, 400);
              done();
            } else {
              done(axiosError);
            }
        })
        .catch(err => done(err));
    });

    test("User attempts to create membership leaving fields blank or wrong chars", done => {
        data["auth_token"] = auth_token;

        data['nombre'] = "Membresía tdd";
        data['descrip'] = "Descripción membresía tdd";
        //data['precio'] = 3312;

        axios.post(`/api/actions/add_membership.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosError => {
            if(axiosError.response){
              let response = axiosError.response;
              assert.equal(response.status, 400);
              done();
            } else {
              done(axiosError);
            }
        })
        .catch(err => done(err));
    });

    test("User succesfully creates membership", done => {
        data["auth_token"] = auth_token;

        data['nombre'] = "Membresía tdd";
        data['descrip'] = "Descripción membresía tdd";
        data['precio'] = 3312;
        data['servicios'] = '';

        axios.post(`/api/actions/add_membership.php`, qs.stringify(data))
        .then(response => {
            assert.equal(response.status, 200);
            done();
        })
        .catch(err => {
          done(err)});
    });

    teardown(done => {
      conn.query(`
        DELETE FROM membresia WHERE nombre LIKE '%tdd%';
      `).then(() => done()).catch(err => done(err));
    });

    suiteTeardown(function (done) {
      conn.query(`
        DELETE FROM Usuario WHERE email = 'crazy4coco@scarface.es';
        DELETE FROM Usuario WHERE email = 'pewdieliza@youtube.com';
        DELETE FROM Rol WHERE nombre = 'test_role';
        DELETE FROM Rol WHERE nombre = 'alien';
      `).then(() => {
        // We close the connection to the database
        return conn.end();
      }).then(() => done()).catch(err => done(err));
    });
});
