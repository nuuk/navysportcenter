// INS43 - ELIMINAR PLAN DE MEMBRESÍA
const { assert } = require("chai");
const conn = require("./utils/db.js").conn();
const axios = require("axios");
const dbadduser = require("./utils/db.js").adduser;
axios.defaults.baseURL = "http://localhost:8080";
const shajs = require("sha.js");
const qs = require("qs");

suite("INS43 - Eliminar plan de membresia", function() {
    var data, auth_token, alt_auth_token;

    suiteSetup(done => {
      conn.connect().then(() => {
        return conn.query(`
        INSERT INTO
        membresia (id, nombre, descripcion, precio)
        VALUES (0, 'pruebaINS43', 'descripcionPrueba', 500);
        `);
      }).then(dbres => {
        return dbadduser(
          conn, {
              nombre: "Tony",
              apellido: "Montana",
              email: "crazy4coco@scarface.es",
              tipo: 0,
              contrasena: shajs("sha256").update("say_hello").digest("hex")
            }, [ 
              "DELETE_MEMBERSHIP"
            ]
        );
      }).then(() => {
        return dbadduser(
          conn, {
              nombre: "Felix",
              apellido: "Koshy",
              email: "pewdieliza@youtube.com",
              tipo: 0,
              contrasena: shajs("sha256").update("say_klk").digest("hex")
          }, [
            "ADD_EMPLOYEE"
          ],
          "alien"
        );
      }).then(() => {
        return axios.post("/api/actions/login.php", qs.stringify({
          email: 'crazy4coco@scarface.es',
          password: 'say_hello'
        }));
      }).then(response => {
        auth_token = response.data.data.auth_token;
        return axios.post("/api/actions/login.php", qs.stringify({
          email: 'pewdieliza@youtube.com',
          password: 'say_klk'
        }));
      }).then(response => {
          alt_auth_token = response.data.data.auth_token;
          done();
      }).catch(err => done(err));
    });

    setup(function(){
        data = {};
    });

    test("user without authentication returns 401", done => {
        axios.post(`/api/actions/delete_membership.php`)
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosErr => {
          if(axiosErr.response){
            let response = axiosErr.response;
            assert.equal(response.status, 401);
            done();
          } else {
            done(axiosErr);
          }
        })
        .catch(err => done(err));
    });

    test("user without authentication returns correct message", done => {
        axios.post(`/api/actions/delete_membership.php`)
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosErr => {
          if(axiosErr.response){
            let response = axiosErr.response;
            assert.equal(response.data.message, `Not authenticated`);
            done();
          } else {
            done(axiosErr);
          }
        })
        .catch(err => done(err));
    });

    test("user doesn't have permissions returns 403", done => {
        data["auth_token"] =  alt_auth_token;
        axios.post(`/api/actions/delete_membership.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosErr => {
          if(axiosErr.response){
            let response = axiosErr.response;
            assert.equal(response.status, 403);
            done();
          } else {
            done(axiosErr);
          }
        })
        .catch(err => done(err));
    });

    test("user doesn't have permissions returns correct message", done => {
        data["auth_token"] =  alt_auth_token;
        axios.post(`/api/actions/delete_membership.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosErr => {
          if(axiosErr.response){
            let response = axiosErr.response;
            assert.equal(response.data.message, `Permission denied`);
            done();
          } else {
            done(axiosErr);
          }
        })
        .catch(err => done(err));
    });

    test("user succesfully deletes membership returns 200", done => {
      data["auth_token"] = auth_token;
      data["id"] = 0;
      axios.post(`/api/actions/delete_membership.php`, qs.stringify(data))
      .then(response => {
        assert.equal(response.status, 200);
        done();
      })
      .catch(err => done(err));
    });

    suiteTeardown(done => {
        conn.query(`
          DELETE FROM membresia WHERE nombre = 'pruebaINS43';
          DELETE FROM usuario WHERE email LIKE '%crazy4coco%';
          DELETE FROM usuario WHERE email LIKE '%pewdieliza%';
          DELETE FROM rol where nombre = 'test_role';
          DELETE FROM rol where nombre = 'alien';
       `)
        .then(dbres => {return conn.end()})
        .then(dbres => done())
        .catch(err => done(err));
    });

});
