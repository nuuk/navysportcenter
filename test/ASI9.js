const { assert } = require("chai"); // We will be using Chai's assert library. https://www.chaijs.com/guide/styles/#assert
const conn = require("./utils/db.js").conn(); // We will also be relying on some utils functions for database connections.
const dbadduser = require("./utils/db.js").adduser;
const axios = require("axios"); // We will use Axios for asynchronous calls. https://www.npmjs.com/package/axios
axios.defaults.baseURL = "http://localhost:8080";

const shajs = require("sha.js");
const qs = require("qs");

suite("ASI9 - Eliminar asistencia", function(){
    // We declare the variable where we'll store our client info.
    var data, auth_token, alt_auth_token, client_id;
  
    /* Sometimes for tests we would need to prepare our enviroment
     * to meet some prerequisites, e.g. to check whether
     * the user has the necessary permissions or not, we must first
     * create the user. */
    suiteSetup(function(done){
  
      /* We start the connection to the database and since this is 
       * an async request we use await, to wait for it to finish. */
      conn.connect().then(() => {
        // We prepare a test user
        return dbadduser(
          conn, {
              nombre: "Ben",
              apellido: "Dove",
              email: "whosyourcookie@gmail.com",
              tipo: 2,
              contrasena: shajs("sha256").update("123").digest("hex")
            }, [ 
              "REGISTER_ATTENDANCE",
              "DELETE_ATTENDANCE",
              "GET_CLIENTS"
            ]
        );
      }).then(dbres => {
        client_id = dbres.rows[0].id_usuario;
        return dbadduser(
          conn, {
              nombre: "Felix",
              apellido: "Koshy",
              email: "pewdieliza@youtube.com",
              tipo: 0,
              contrasena: shajs("sha256").update("say_klk").digest("hex")
          }, [
            "GET_CLIENTS"
        ], "alien");
      }).then(() => {
        return axios.post("/api/actions/login.php", qs.stringify({
          email: 'whosyourcookie@gmail.com',
          password: '123'
        }));
      }).then(response => {
        auth_token = response.data.data.auth_token;
        return axios.post("/api/actions/login.php", qs.stringify({
          email: 'pewdieliza@youtube.com',
          password: 'say_klk'
        }));
      }).then(response => {
        alt_auth_token = response.data.data.auth_token;
        // We prepare a test attendance entry
        return conn.query(`
            INSERT INTO
            asistencia (fecha, tiempo, cliente, estado) 
            VALUES ('2020-01-01', '12:00:00', ${client_id}, 1)
        `);
      }).then(() => done()).catch(err => done(err)); 
  
    });

    setup(function(){
        // We create the dummy data for our new client
        data = {};
        // We set a a valid authentication token
    });

    test("user without authentication returns 401", function(done){
        // We make a POST request with an empty request body.
        axios.post(`/api/actions/bulk_delete_attendees.php`, data)
          // If the request is successfully sent then...
          .then(function(response){
            //It means that the status code is in the 1XX-3XX range, thus we must fail
            assert.fail(`Request succeded with status code ${response.status}.`);
    
            // We tell Mocha we are done.
            done();
    
            // If the request had an error we catch it and...
          }).catch((axiosErr) => {
            // If we got a response...
            if(axiosErr.response){
              // We access the response object in the error
              let response = axiosErr.response;
    
              // We compare the actual response code to our expected 401
              assert.equal(response.status, 401);
    
              // We tell Mocha we are done.
              done();
            } else {
              // ...else report it to Mocha
              done(axiosErr);
            }
          }).catch((assertErr) => {
            // We notify Mocha of any other error.
            done(assertErr);
        });
    });

    test("user without authentication returns correct message", function(done){
        axios.post(`/api/actions/bulk_delete_attendees.php`, data)
          .then(function(response){
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        }).catch((axiosErr) => {
            if(axiosErr.response){
              let response = axiosErr.response;
              assert.equal(response.data.message, "Not authenticated");
              done();
            } else {
              done(axiosErr);
            }
        }).catch((assertErr) => {
            done(assertErr);
        });
    });

    /* M: The user is does not have permissions */
    test("user doesn't have permissions should return 403", function(done){
        data["auth_token"] = alt_auth_token;
        axios.post(`/api/actions/bulk_delete_attendees.php`, qs.stringify(data))
        .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
        .catch(axiosError => {
            if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.status, 403);
            done();
            } else {
            done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test("user doesn't have permissions should return correct message", function(done){
        data["auth_token"] = alt_auth_token;
        axios.post(`/api/actions/bulk_delete_attendees.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosError => {
          if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.data.message, "Permission denied");
            done();
          } else {
            done(axiosError);
          }
        })
        .catch(error => done(error));
    });

    // M: The user enters an invalid date
    // M: The user enters an invalid time
    var fields_to_validate = [
        {name: "date", value: "2019-01-001"},
        {name: "time", value: "102:55:00"}
    ];

    fields_to_validate.forEach((field) => {
        test(`user doesn't enter a valid ${field.name} returns 400`, function(done){
            data["auth_token"] = auth_token;
            data[field.name] = field.value;
            axios.post(`/api/actions/bulk_delete_attendees.php`, qs.stringify(data))
            .then(response => {
              assert.fail(`Request succeded with status code ${response.status}.`);
              done();
            })
            .catch(axiosError => {
              if(axiosError.response){
                let response = axiosError.response;
                assert.equal(response.status, 400);
                done();
              } else {
                done(axiosError);
              }
            })
            .catch(error => done(error));
        });

        test(`user doesn't enter a valid ${field.name} returns the correct message`, function(done){
            data["auth_token"] = auth_token;
            data[field.name] = field.value;
            axios.post(`/api/actions/bulk_delete_attendees.php`, qs.stringify(data))
            .then(response => {
              assert.fail(`Request succeded with status code ${response.status}.`);
              done();
            })
            .catch(axiosError => {
              if(axiosError.response){
                let response = axiosError.response;
                assert.equal(response.data.message.errors[field.name], `Invalid ${field.name}`);
                done();
              } else {
                done(axiosError);
              }
            })
            .catch(error => done(error));
          });
    });

    test(`user enters invalid date returns 400`, function(done) {
        data["auth_token"] = auth_token;
        data["date"] = 'fwwf';
        data["time"] = '12:00:00';
        axios.post(`/api/actions/bulk_delete_attendees.php`, qs.stringify(data))
          .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
          .catch(axiosError => {
            if(axiosError.response){
              let response = axiosError.response;
              assert.equal(response.status, 400);
              done();
            } else {
              done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`user enters invalid date returns correct message`, function(done) {
        data["auth_token"] = auth_token;
        data["date"] = 'fwfw';
        data["time"] = '12:00:00';
        axios.post(`/api/actions/bulk_delete_attendees.php`, qs.stringify(data))
        .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
        .catch(axiosError => {
            if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.data.message.errors['date'], `Invalid date`);
            done();
            } else {
            done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`user enters invalid time returns 400`, function(done) {
        data["auth_token"] = auth_token;
        data["date"] = '2020-01-01';
        data["time"] = 'fwfw';
        axios.post(`/api/actions/bulk_delete_attendees.php`, qs.stringify(data))
          .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
          .catch(axiosError => {
            if(axiosError.response){
              let response = axiosError.response;
              assert.equal(response.status, 400);
              done();
            } else {
              done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`user enters invalid date returns correct message`, function(done) {
        data["auth_token"] = auth_token;
        data["date"] = '2020-01-01';
        data["time"] = 'fwfw';
        axios.post(`/api/actions/bulk_delete_attendees.php`, qs.stringify(data))
        .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
        .catch(axiosError => {
            if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.data.message.errors['time'], `Invalid time`);
            done();
            } else {
            done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`user enters valid date and time returns 200`, function(done) {
        data["auth_token"] = auth_token;
        data["date"] = '2020-01-01';
        data["time"] = '12:00:00';
        data["clients"] = JSON.stringify([client_id])
        axios.post("/api/actions/bulk_delete_attendees.php", qs.stringify(data))
          .then(response => {
            assert.equal(response.status, 200);
            return conn.query(`
                INSERT INTO
                asistencia (fecha, tiempo, cliente, estado) 
                VALUES ('2020-01-01', '12:00:00', ${client_id}, 1)
            `)
          })
          .then(cleanup => {
            done();
          })
          .catch(err => done(err));
    });

    test(`user enters valid date and time returns correct message`, function(done) {
        data["auth_token"] = auth_token;
        data["date"] = '2020-01-01';
        data["time"] = '12:00:00';
        data["clients"] = JSON.stringify([client_id])
        axios.post("/api/actions/bulk_delete_attendees.php", qs.stringify(data))
          .then(response => {
            assert.equal(response.data.message, "Attendance deleted successfully");
            return conn.query(`
                INSERT INTO
                asistencia (fecha, tiempo, cliente, estado) 
                VALUES ('2020-01-01', '12:00:00', ${client_id}, 1)
            `);
          })
          .then(cleanup => {
            done();
          })
          .catch(err => done(err));
    });

    test(`user enters valid date and time deletes record from db`, function(done) {
        data["auth_token"] = auth_token;
        data["date"] = '2020-01-01';
        data["time"] = '12:00:00';
        data["clients"] = JSON.stringify([client_id]);
        axios.post("/api/actions/bulk_delete_attendees.php", qs.stringify(data))
        .then(response => {
            return conn.query(`
                SELECT *
                FROM asistencia
                WHERE id = ${client_id};
            `)
        })
        .then(dbres => {
            assert.lengthOf(dbres.rows, 0)
            return conn.query(`
                INSERT INTO
                asistencia (fecha, tiempo, cliente, estado) 
                VALUES ('2020-01-01', '12:00:00', ${client_id}, 1)
            `);
        })
        .then(cleanup => {
            done();
        })
        .catch(err => done(err));
    });

    suiteTeardown(function(done){
        // We delete the test attendance entry and user
        conn.query(`
            DELETE FROM asistencia WHERE cliente = ${client_id}
        `);

        conn.query(`
            DELETE FROM usuario WHERE id = ${client_id};
            DELETE FROM usuario WHERE email LIKE '%pewdie%';
            DELETE FROM Rol WHERE nombre = 'test_role';
            DELETE FROM Rol WHERE nombre = 'alien';
        `).then(() => {
            // We close the connection to the database
            return conn.end();
        }).then(() => done()).catch(err => done(err));
    });

});
