const { Client } = require('pg');
const DEVELOPMENT = true;
const LOCAL = process.env.NSC_LOCAL_ENV == 1 ? true : false;

const clnt = function(){
  let host = "";
  let database = "";
  let user = "";
  let password = "";
  let ssl = false;
  if(DEVELOPMENT){
    host =  "ec2-54-83-33-14.compute-1.amazonaws.com";
    database = "d65ctciesds13s";
    user = "vdirbrhpwqyjgg";
    password = process.env.NSC_DEVDB_PWD;
    ssl = true;
  }
  if(LOCAL){
    host =  process.env.NSC_LCDB_HOST;
    database = process.env.NSC_LCDB_DB;
    user = process.env.NSC_LCDB_USER;
    password = process.env.NSC_LCDB_PWD;
    ssl = false;
  }
  return new Client({
    host,
    database,
    user,
    password,
    ssl
  });
}

module.exports = {
  conn: clnt,
  adduser: function(connection, user, permissions, rol){
    if(rol == undefined){
      rol = "test_role";
    }
    
    required_fields = [
      "nombre",
      "apellido",
      "email",
      "tipo",
      "contrasena"
    ];
    return new Promise((resolve, reject) => {
      debugger;
      required_fields.forEach(field => {
        if(user[field] == undefined){
          reject(`Missing ${field}`);
        }
      });
      let user_id, role_id;
      connection.query(
      `INSERT INTO Usuario
        (nombre, apellido, email, tipo, contrasena)
        VALUES (
          '${user.nombre}', 
          '${user.apellido}', 
          '${user.email}', 
          ${user.tipo},
          '${user.contrasena}'
        ) RETURNING id;`
      )
        .then(dbres => {
          user_id = dbres.rows[0].id;
          return connection.query(
            `SELECT id FROM Rol WHERE nombre = '${rol}';`
          );
        })
        .then(dbres => {
          if(!dbres.rows[0]){
            return connection.query(
            `INSERT INTO Rol (nombre) VALUES (
              '${rol}'
              ) RETURNING id;`
            );
          }
          return new Promise((res, rej) => res(dbres));
        })
        .then(dbres => {
          role_id = dbres.rows[0].id;
          if(permissions != undefined && permissions.length > 0){
            let query = `SELECT slug FROM Permiso WHERE `;
            permissions.forEach((permission, idx) => {
              query += `slug = '${permission}'`;
              if(idx < permissions.length - 1){
                query += `OR  `;
              }
            });
            return connection.query(query);
          }
          resolve();
        })
        .then(dbres => {
          if(dbres.rows != undefined && dbres.rows.length > 0){
            let query = `INSERT INTO Rol_Permiso (id_rol, id_permiso) VALUES `;
            dbres.rows.forEach((record, idx) => {
              query += `(${role_id}, '${record.slug}')` 
              if(idx < dbres.rows.length - 1){
                query += `, `;
              }
            });
            query += " ON CONFLICT DO NOTHING;";
            return connection.query(query);
          }
          return new Promise((res, rej) => {res()});
        })
        .then(dbres => {
          return connection.query(
            `INSERT INTO Usuario_Rol (id_usuario, id_rol)
            VALUES (${user_id}, ${role_id}) RETURNING *;`
          );
        })
        .then(dbres => resolve(dbres))
        .catch(err => reject(err));
    });
  }
}

