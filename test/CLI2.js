const { assert } = require("chai");
const conn = require("./utils/db.js").conn();
const dbadduser = require("./utils/db.js").adduser;
const axios = require("axios");
axios.defaults.baseURL = "http://localhost:8080";
const shajs = require("sha.js");
const qs = require("qs");

suite("CLI2 - Editar cliente", () => {
  var data, auth_token, alt_auth_token, client_id, employee_id;
  let edit_url = "/api/actions/edit_client.php?id=";

  suiteSetup(done => {
    conn.connect().then(() => {
      return dbadduser(
        conn, {
            nombre: "Tony",
            apellido: "Montana",
            email: "crazy4coco@scarface.es",
            tipo: 1,
            contrasena: shajs("sha256").update("say_hello").digest("hex")
          }, [ 
            "EDIT_CLIENT"
          ]
      );
    }).then(dbres => {
      client_id = dbres.rows[0].id_usuario;
      return dbadduser(
        conn, {
            nombre: "Sujeto",
            apellido: "Oro",
            email: "llegolacena@sprout.bio",
            tipo: 0,
            contrasena: shajs("sha256").update("say_klk").digest("hex")
        }, [
          "EDIT_CLIENT",
        ],
      );
    }).then(dbres => {
      employee_id = dbres.rows[0].id_usuario;
      return dbadduser(
        conn, {
            nombre: "Felix",
            apellido: "Koshy",
            email: "pewdieliza@youtube.com",
            tipo: 0,
            contrasena: shajs("sha256").update("say_klk").digest("hex")
        }, [
          "ADD_EMPLOYEE"
        ],
        "alien"
      );
    }).then(() => {
      return axios.post("/api/actions/login.php", qs.stringify({
        email: 'crazy4coco@scarface.es',
        password: 'say_hello'
      }));
    }).then(response => {
      auth_token = response.data.data.auth_token;
      return axios.post("/api/actions/login.php", qs.stringify({
        email: 'pewdieliza@youtube.com',
        password: 'say_klk'
      }));
    }).then(response => {
        alt_auth_token = response.data.data.auth_token;
        edit_url += client_id;
        done()
    }).catch(err => done(err));
  });

  setup(() => {
    data = {};
    data["auth_token"] = auth_token;
  });

  test("user without authentication returns 401", done => {
    axios.post(edit_url)
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.status, 401);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user without authentication returns correct message", done => {
    axios.post(edit_url)
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.data.message, `Not authenticated`);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user doesn't have permissions returns 403", done => {
    data["auth_token"] = alt_auth_token;
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.status, 403);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user doesn't have permissions returns correct message", done => {
    data["auth_token"] = alt_auth_token;
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.data.message, `Permission denied`);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  let invalid_fields = [
    {name: "email", value: "klk"},
    {name: "phone", value: "123"},
    {name: "first_name", value: "123S&S"},
    {name: "last_name", value: "123S&S"}
  ];

  test("various fields don't accept invalid values returns 400", done => {
    invalid_fields.forEach(field => {
      data[field.name] = field.value;
    });
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.fail(`Request failed with status code ${response.status}`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.status, 400);
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(err => done(err));
  });

  test("various fields with invalid values return correct messages", done => {
    invalid_fields.forEach(field => {
      data[field.name] = field.value;
    });

    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.fail(`Request failed with status code ${response.status}`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        invalid_fields.forEach(field => {
          assert.equal(response.data.message.errors[field.name], `Invalid ${field.name}`);
        });
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(err => done(err));
  });

  invalid_fields.forEach(field => {
    test(`the ${field.name} field doesn't accept invalid value returns 400`, done => {
      data[field.name] = field.value;
      axios.post(edit_url, qs.stringify(data))
        .then(response => {
          assert.fail(`Request failed with status code ${response.status}`);
          done();
        })
        .catch(axiosError => {
          if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.status, 400);
            done();
          } else {
            done(axiosError);
          }
        })
        .catch(err => done(err));
        })

    test(`the ${field.name} field doesn't accept invalid value returns message`, done => {
      data[field.name] = field.value;
      axios.post(edit_url, qs.stringify(data))
        .then(response => {
          assert.fail(`Request failed with status code ${response.status}`);
          done();
        })
        .catch(axiosError => {
          if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.data.message.errors[field.name], `Invalid ${field.name}`);
            done();
          } else {
            done(axiosError);
          }
        })
        .catch(err => done(err));
      })
  });

  let partial_update_fields = [
    {name: "email", value: "elconejo@sanjuan.pr", db_name: "email"},
    {name: "phone", value: "52 442 123 4578", db_name: "telefono"},
    {name: "first_name", value: "Alèxîs", db_name: "nombre"},
    {name: "last_name", value: "Günther Yákult", db_name: "apellido"}
  ];
  
  partial_update_fields.forEach(field => {
    test(`partial update of ${field.name} returns 200`, done => {
      data[field.name] = field.value;
      axios.post(edit_url, qs.stringify(data))
      .then(response => {
        assert.equal(response.status, 200);
        done();
      })
      .catch(err => done(err));
    });

    test(`partial update of ${field.name} returns correct message`, done => {
      data[field.name] = field.value;
      axios.post(edit_url, qs.stringify(data))
      .then(response => {
        assert.equal(response.data.message, "Client updated successfully.");
        done();
      })
      .catch(err => done(err));
    });

    test(`partial update of ${field.name} is reflected in the db`, done => {
      data[field.name] = field.value;
      axios.post(edit_url, qs.stringify(data))
      .then(response => {
        return conn.query(`
          SELECT ${field.db_name} 
          FROM Usuario
          WHERE id = ${client_id};
        `);
      })
      .then(dbres => {
        let value = field.value.trim();
        if(field.name == "phone"){
          value = field.value.trim().replace(/ /g, "");
        }
        assert.equal(dbres.rows[0][field.db_name], value);
        done();
      })
      .catch(err => done(err));
    });
  });

  test("full update returns 200", done => {
    let email = "elconejomalo@sanjuan.pr";
    let phone = "422 433 3245";
    let first_name = "Benito";
    let last_name = "Martínez";
    data["email"] = email;
    data["phone"] = phone;
    data["first_name"] = first_name;
    data["last_name"] = last_name;
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.equal(response.status, 200);
      done();
    })
    .catch(err => done(err));
  });

  test("full update returns the correct message", done => {
    let email = "elconejomalo@sanjuan.pr";
    let phone = "422 433 3245";
    let first_name = "benito";
    let last_name = "martínez";
    data["email"] = email;
    data["phone"] = phone;
    data["first_name"] = first_name;
    data["last_name"] = last_name;
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.equal(response.data.message, "Client updated successfully.");
      done();
    })
    .catch(err => done(err));
  });

  test("full update is reflected in the db", done => {
    let params = [
      {name: "email", value: "elconejomalo@sanjuan.pr", db_field: "email"},
      {name: "phone", value: "422 433 3245", db_field: "telefono"},
      {name: "first_name", value: "Benito", db_field: "nombre"},
      {name: "last_name", value: "Martínez", db_field: "apellido"}
    ];
    data["email"] = params[0].value;
    data["phone"] = params[1].value;
    data["first_name"] = params[2].value;
    data["last_name"] = params[3].value;
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      return conn.query(`
        SELECT * FROM Usuario WHERE id = ${client_id}
      `);
    })
    .then(dbres => {
      params.forEach(param => {
        let value = param.value.trim();
        if(param.name == "phone"){
          value = param.value.trim().replace(/ /g, "");
        }
        assert.equal(dbres.rows[0][param.db_field], value);
      });
      done();
    })
    .catch(err => done(err));
  });

  test("full update w/ empty phone returns 200", done => {
    let email = "elconejomalo@sanjuan.pr";
    let phone = "";
    let first_name = "Benito";
    let last_name = "Martínez";
    data["email"] = email;
    data["phone"] = phone;
    data["first_name"] = first_name;
    data["last_name"] = last_name;
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.equal(response.status, 200);
      done();
    })
    .catch(err => done(err));
  });

  test("full update w/ empty phone returns the correct message", done => {
    let email = "elconejomalo@sanjuan.pr";
    let phone = "";
    let first_name = "benito";
    let last_name = "martínez";
    data["email"] = email;
    data["phone"] = phone;
    data["first_name"] = first_name;
    data["last_name"] = last_name;
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.equal(response.data.message, "Client updated successfully.");
      done();
    })
    .catch(err => done(err));
  });

  test("full update w/ empty phone is reflected in the db", done => {
    let params = [
      {name: "email", value: "elconejomalo@sanjuan.pr", db_field: "email"},
      {name: "phone", value: "", db_field: "telefono"},
      {name: "first_name", value: "Benito", db_field: "nombre"},
      {name: "last_name", value: "Martínez", db_field: "apellido"}
    ];
    data["email"] = params[0].value;
    data["phone"] = params[1].value;
    data["first_name"] = params[2].value;
    data["last_name"] = params[3].value;
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      return conn.query(`
        SELECT * FROM Usuario WHERE id = ${client_id}
      `);
    })
    .then(dbres => {
      params.forEach(param => {
        let value = param.value.trim();
        if(param.name == "phone"){
          value = param.value.trim().replace(/ /g, "");
        }
        assert.equal(dbres.rows[0][param.db_field], value);
      });
      done();
    })
    .catch(err => done(err));
  });

  test("SQL injection is rejected with 400", done => {
    data["email"] = `;DELETE FROM Usuario WHERE id = ${client_id};`;
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.status, 400);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  })

  test("SQL injection is rejected doesnt affect db", done => {
    data["email"] = `;DELETE FROM Usuario WHERE id = ${client_id};`;
    axios.post(edit_url, qs.stringify(data))
    .then(response => {})
    .catch(err => {})
    .finally(() => {
      conn.query(`SELECT id FROM Usuario WHERE id = ${client_id};`)
      .then(dbres => {
        assert.lengthOf(dbres.rows, 1);
        done();
      })
    })
  })

  test("empty request returns 200", done => {
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.equal(response.status, 200);
      done();
    })
    .catch(err => done(err));
  });

  test("empty request returns correct message", done => {
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.equal(response.data.message, "No changes detected");
      done();
    })
    .catch(err => done(err));
  });

  test("empty request has no effect on db", done => {
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      return conn.query(`
      SELECT * FROM Usuario WHERE id = ${client_id};
      `);
    })
    .then(dbres => {
      assert.equal(dbres.rows[0].email,'crazy4coco@scarface.es');
      assert.equal(dbres.rows[0].nombre,'Tony');
      assert.equal(dbres.rows[0].apellido,'Montana');
      done();
    })
    .catch(err => done(err));
  });

  test("user missing id returns 400", done => {
    axios.post("/api/actions/edit_client.php", qs.stringify(data))
    .then(response => {
      assert.fail(`Request failed with status code ${response.status}`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.status, 400);
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(err => done(err));
  });

  test("user missing id returns correct message", done => {
    axios.post("/api/actions/edit_client.php", qs.stringify(data))
    .then(response => {
      assert.fail(`Request failed with status code ${response.status}`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.data.message.errors.id, "The id is required");
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(err => done(err));
  });

  test("user bad id returns 400", done => {
    axios.post("/api/actions/edit_client.php?id=klk", qs.stringify(data))
    .then(response => {
      assert.fail(`Request failed with status code ${response.status}`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.status, 400);
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(err => done(err));
  });

  test("user bad id returns correct message", done => {
    axios.post("/api/actions/edit_client.php?id=klk", qs.stringify(data))
    .then(response => {
      assert.fail(`Request failed with status code ${response.status}`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.data.message.errors.id, "Invalid id");
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(err => done(err));
  });

  test("user not found returns 404", done => {
    axios.post("/api/actions/edit_client.php?id=99969429999", qs.stringify(data))
    .then(response => {
      assert.fail(`Request failed with status code ${response.status}`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.status, 404);
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(err => done(err));
  });

  test("user not found returns correct message", done => {
    axios.post("/api/actions/edit_client.php?id=99969429999", qs.stringify(data))
    .then(response => {
      assert.fail(`Request failed with status code ${response.status}`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.data.message, "Client not found.");
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(err => done(err));
  });

  test("user not a client returns 404", done => {
    axios.post(`/api/actions/edit_client.php?id=${employee_id}`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request failed with status code ${response.status}`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.status, 404);
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(err => done(err));
  });

  test("user not a client returns correct message", done => {
    axios.post(`/api/actions/edit_client.php?id=${employee_id}`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request failed with status code ${response.status}`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.data.message, "Client not found.");
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(err => done(err));
  });

  test("partial update to existing email returns 400", done => {
    data["email"] = "llegolacena@sprout.bio";
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.fail(`Request failed with status code ${response.status}`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.status, 400);
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(err => done(err));
  });

  test("partial update to existing email returns correct message", done => {
    data["email"] = "llegolacena@sprout.bio";
    axios.post(edit_url, qs.stringify(data))
    .then(response => {
      assert.fail(`Request failed with status code ${response.status}`);
      done();
    })
    .catch(axiosError => {
      if(axiosError.response){
        let response = axiosError.response;
        assert.equal(response.data.message, "Email already in use");
        done();
      } else {
        done(axiosError);
      }
    })
    .catch(err => done(err));
  });

  teardown(done => {
    conn.query(`
        UPDATE Usuario
        SET nombre = 'Tony',
        apellido = 'Montana',
        email = 'crazy4coco@scarface.es'
        WHERE id = ${client_id};
    `)
    .then(dbres => done())
    .catch(err => done(err));
  });

  suiteTeardown(done => {
    conn.query(`
        DELETE FROM Usuario WHERE email = 'crazy4coco@scarface.es';
        DELETE FROM Usuario WHERE email = 'llegolacena@sprout.bio';
        DELETE FROM Usuario WHERE email = 'pewdieliza@youtube.com';
        DELETE FROM Rol WHERE nombre = 'test_role';
        DELETE FROM Rol WHERE nombre = 'alien';
    `)
      .then(() => {
        return conn.end();
      })
      .then(() => done())
      .catch(err => done(err));
  });

});
