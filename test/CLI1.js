/* CLI1 - Crear cliente */

/* In order to develop this use case we need to first create its test
 * this is why before writing any functional code we must write a
 * functional test. Since many interfaces have been already created
 * this tests will focus on testing the API service. This means that
 * we will always assume that the user is already in the corresponding 
 * view. */

/* We include dependencies (as constants to avoid accidental 
 * variable overwriting) that will help us develop the tests */
const { assert } = require("chai"); // We will be using Chai's assert library. https://www.chaijs.com/guide/styles/#assert
const conn = require("./utils/db.js").conn(); // We will also be relying on some utils functions for database connections.
const dbadduser = require("./utils/db.js").adduser;
const axios = require("axios"); // We will use Axios for asynchronous calls. https://www.npmjs.com/package/axios
axios.defaults.baseURL = "http://localhost:8080";

const shajs = require("sha.js");
const qs = require("qs");

/* It is recommended to write in comments the paths/possible outcomes
 * of your use case for this example we will identify the path as:
 * H: Happy Path - no errors, the user does what we expect
 * M: Messy Path - user errors where the user enters invalid inputs
 * F: Foobar Path - server errors or unmet preconditions, e.g. a resource doesn't exist 
 *
 * [NOTE: Here's the explanation for choosing foobar (https://en.wikipedia.org/wiki/List_of_military_slang_terms#FUBAR)] */


/* To group together all tests for CLI1 we declare a suite */
suite("CLI1 - Crear cliente", function () {
  // We declare the variable where we'll store our client info.
  var data, auth_token, alt_auth_token;

  /* Sometimes for tests we would need to prepare our enviroment
   * to meet some prerequisites, e.g. to check whether
   * the user has the necessary permissions or not, we must first
   * create the user. */
  suiteSetup(function (done) {

    /* We start the connection to the database and since this is 
     * an async request we use await, to wait for it to finish. */
    conn.connect().then(() => {
      return dbadduser(
        conn, {
            nombre: "Tony",
            apellido: "Montana",
            email: "crazy4coco@scarface.es",
            tipo: 0,
            contrasena: shajs("sha256").update("say_hello").digest("hex")
          }, [ 
            "ADD_CLIENT"
          ]
      );
    }).then(() => {
      return dbadduser(
        conn, {
            nombre: "Felix",
            apellido: "Koshy",
            email: "pewdieliza@youtube.com",
            tipo: 0,
            contrasena: shajs("sha256").update("say_klk").digest("hex")
        }, [
          "ADD_EMPLOYEE"
        ],
        "alien"
      );
    }).then(() => {
      return axios.post("/api/actions/login.php", qs.stringify({
        email: 'crazy4coco@scarface.es',
        password: 'say_hello'
      }));
    }).then(response => {
      auth_token = response.data.data.auth_token;
      return axios.post("/api/actions/login.php", qs.stringify({
        email: 'pewdieliza@youtube.com',
        password: 'say_klk'
      }));
    }).then(response => {
        alt_auth_token = response.data.data.auth_token;
        done();
    }).catch(err => {done(err);});
  });

  // We define a preparation script to be ran before each test.
  setup(() => {
    // We create the dummy data for our new client
    data = {};
  });

  /* M: The user is not authenticated 
   *
   * [NOTE: Since we are going to run asynchronous code, we need
   * to pass the test function the done paramter to later notify
   * Mocha when we are done with our test]
   * */
  test("user without authentication returns 401", function (done) {
    // We make a POST request with an empty request body.
    axios.post(`/api/actions/create_client.php`, data)
      // If the request is successfully sent then...
      .then(function (response) {
        //It means that the status code is in the 1XX-3XX range, thus we must fail
        assert.fail(`Request succeded with status code ${response.status}.`);

        // We tell Mocha we are done.
        done();

        // If the request had an error we catch it and...
      }).catch((axiosErr) => {
        // If we got a response...
        if (axiosErr.response) {
          // We access the response object in the error
          let response = axiosErr.response;

          // We compare the actual response code to our expected 401
          assert.equal(response.status, 401);

          // We tell Mocha we are done.
          done();
        } else {
          // ...else report it to Mocha
          done(axiosErr);
        }
      }).catch((assertErr) => {
        // We notify Mocha of any other error.
        done(assertErr);
      });
  });

  test("user without authentication returns correct message", function (done) {
    axios.post(`/api/actions/create_client.php`, data)
      .then(function (response) {
        assert.fail(`Request succeded with status code ${response.status}.`);
        done();
      }).catch((axiosErr) => {
        if (axiosErr.response) {
          let response = axiosErr.response;
          assert.equal(response.data.message, "Not authenticated");
          //assert.equal(response.data.message, "authenticated"); // para mostrar como funciona tdd
          done();
        } else {
          done(axiosErr);
        }
      }).catch((assertErr) => {
        done(assertErr);
      });
  });

  /* M: The user is does not have permissions */
  test("user doesn't have permissions should return 403", function (done) {
    data["auth_token"] = alt_auth_token;
    axios.post(`/api/actions/create_client.php`, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeded with status code ${response.status}.`);
        done();
      })
      .catch(axiosError => {
        if (axiosError.response) {
          let response = axiosError.response;
          assert.equal(response.status, 403);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(error => done(error));
  });

  test("user doesn't have permissions should return correct message", function (done) {
    data["auth_token"] = alt_auth_token;
    axios.post(`/api/actions/create_client.php`, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeded with status code ${response.status}.`);
        done();
      })
      .catch(axiosError => {
        if (axiosError.response) {
          let response = axiosError.response;
          assert.equal(response.data.message, "Permission denied");
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(error => done(error));
  });

  /* M: The user enters an invalid email */
  /* M: The user enters an invalid cellphone */
  var fields_to_validate = [
    { name: "email", value: "klk" },
    { name: "phone", value: "kl9021k" }
  ]

  fields_to_validate.forEach((field) => {
    test(`user doesn't enter a valid ${field.name} returns 400`, function (done) {
      data["auth_token"] = auth_token;
      data[field.name] = field.value
      axios.post(`/api/actions/create_client.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosError => {
          if (axiosError.response) {
            let response = axiosError.response;
            assert.equal(response.status, 400);
            done();
          } else {
            done(axiosError);
          }
        })
        .catch(error => done(error));
    });

    test(`user doesn't enter a valid ${field.name} returns the correct message`, function (done) {
      data["auth_token"] = auth_token;
      data[field.name] = field.value;
      axios.post(`/api/actions/create_client.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosError => {
          if (axiosError.response) {
            let response = axiosError.response;
            assert.equal(response.data.message.errors[field.name], `Invalid ${field.name}`);
            done();
          } else {
            done(axiosError);
          }
        })
        .catch(error => done(error));
    });
  });

  /* M: The user enters a duplicate email */
  test(`user enter an existing email returns 400`, function (done) {
    data["auth_token"] = auth_token;
    data["first_name"] = 'Tony';
    data["last_name"] = 'Montana';
    data["email"] = 'crazy4coco@scarface.es';
    axios.post(`/api/actions/create_client.php`, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeded with status code ${response.status}.`);
        done();
      })
      .catch(axiosError => {
        if (axiosError.response) {
          let response = axiosError.response;
          assert.equal(response.status, 400);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(error => done(error));
  });

  test(`user enter an existing email returns the correct message`, function (done) {
    data["auth_token"] = auth_token;
    data["first_name"] = 'Tony';
    data["last_name"] = 'Montana';
    data["email"] = 'crazy4coco@scarface.es';
    axios.post(`/api/actions/create_client.php`, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeded with status code ${response.status}.`);
        done();
      })
      .catch(axiosError => {
        if (axiosError.response) {
          let response = axiosError.response;
          assert.equal(response.data.message, `Email already in use`);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(error => done(error));
  });

  /* M: The user includes more than one field with errors */
  test("user doesn't enter a valid cellphone nor email returns 400", function (done) {
    data["auth_token"] = auth_token;

    data["email"] = "klk";
    data["phone"] = "96024960249602496024960249602496024960249602496024";

    axios.post(`/api/actions/create_client.php`, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeded with status code ${response.status}.`);
        done();
      })
      .catch(axiosError => {
        if (axiosError.response) {
          let response = axiosError.response;
          assert.equal(response.status, 400);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(error => done(error));
  });
  test("user doesn't enter a valid cellphone nor email returns correct message", function (done) {
    data["auth_token"] = auth_token;

    data["email"] = "klk";
    data["phone"] = "96024960249602496024960249602496024960249602496024";

    axios.post(`/api/actions/create_client.php`, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeded with status code ${response.status}.`);
        done();
      })
      .catch(axiosError => {
        if (axiosError.response) {
          let response = axiosError.response;
          assert.equal(response.data.message.errors.email, "Invalid email");
          assert.equal(response.data.message.errors.phone, "Invalid phone");
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(error => done(error));
  });

  /* M: The user didn't include at least, email, first name and last name*/
  var required_fields = [
    { name: "email" },
    { name: "first_name" },
    { name: "last_name" }
  ];
  required_fields.forEach((field) => {
    test(`user doesn't enter a ${field.name} returns 400`, function (done) {
      data["auth_token"] = auth_token;
      axios.post(`/api/actions/create_client.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosError => {
          if (axiosError.response) {
            let response = axiosError.response;
            assert.equal(response.status, 400);
            done();
          } else {
            done(axiosError);
          }
        })
        .catch(error => done(error));
    });
    test(`user doesn't enter a ${field.name} returns correct message`, function (done) {
      data["auth_token"] = auth_token;

      axios.post(`/api/actions/create_client.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosError => {
          if (axiosError.response) {
            let response = axiosError.response;
            assert.equal(response.data.message.errors[field.name], `The ${field.name} is required`);
            done();
          } else {
            done(axiosError);
          }
        })
        .catch(error => done(error));
    });
  });

  test("user doesn't enter a valid name, nor last_name nor email returns 400", function (done) {
    data["auth_token"] = auth_token;

    axios.post(`/api/actions/create_client.php`, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeded with status code ${response.status}.`);
        done();
      })
      .catch(axiosError => {
        if (axiosError.response) {
          let response = axiosError.response;
          assert.equal(response.status, 400);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(error => done(error));
  });

  test("user doesn't enter a valid name, nor last_name nor email returns correct message", function (done) {
    data["auth_token"] = auth_token;

    axios.post(`/api/actions/create_client.php`, qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeded with status code ${response.status}.`);
        done();
      })
      .catch(axiosError => {
        if (axiosError.response) {
          let response = axiosError.response;
          required_fields.forEach((field) => {
            assert.equal(response.data.message.errors[field.name], `The ${field.name} is required`);
          });
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(error => done(error));
  });

  /* H: The user enters valid client information */
  test("user enters valid user returns 201", function(done){
    let first_name = "Mîchélè Cobra";
    let last_name = "Cávataio Fêrry";
    let email = "thecobra@cavataio.com";
    data["auth_token"] = auth_token;
    data["first_name"] = first_name;
    data["last_name"] = last_name;
    data["email"] = email;

    axios.post("/api/actions/create_client.php", qs.stringify(data))
      .then(response => {
        assert.equal(response.status, 201);
        done();
      })
      .catch(err => done(err))
  });

  test("user enters valid user returns user created message", function (done) {
    let first_name = "Mîchélè Cobra";
    let last_name = "Cávataio Fêrry";
    let email = "thecobra@cavataio.com";
    data["auth_token"] = auth_token;
    data["first_name"] = first_name;
    data["last_name"] = last_name;
    data["email"] = email;

    axios.post("/api/actions/create_client.php", qs.stringify(data))
      .then(response => {
        assert.equal(response.data.message, "Client created successfully");
        done();
      })
      .catch(err => done(err))
  });

  test("user enters valid user creates record in db", function (done) {
    let first_name = "Mîchélè Cobra";
    let last_name = "Cávataio Fêrry";
    let email = "thecobra@cavataio.com";
    data["auth_token"] = auth_token;
    data["first_name"] = first_name;
    data["last_name"] = last_name;
    data["email"] = email;

    axios.post("/api/actions/create_client.php", qs.stringify(data))
      .then(response => {
        return conn.query(`
      SELECT *
      FROM Usuario
      WHERE nombre = '${first_name}'
      AND apellido = '${last_name}'
      AND email = '${email}';
      `);
      })
      .then(dbres => {
        assert.lengthOf(dbres.rows, 1)
        done();
      })
      .catch(err => done(err))
  });

  test("user enters valid user w/ empty phone returns 201", function(done){
    let first_name = "Mîchélè Cobra";
    let last_name = "Cávataio Fêrry";
    let email = "thecobra@cavataio.com";
    data["auth_token"] = auth_token;
    data["first_name"] = first_name;
    data["last_name"] = last_name;
    data["email"] = email;
    data["phone"] = "";

    axios.post("/api/actions/create_client.php", qs.stringify(data))
      .then(response => {
        assert.equal(response.status, 201);
        done();
      })
      .catch(err => done(err))
  });

  test("user enters valid user w/ empty phone returns created message", function (done) {
    let first_name = "Mîchélè Cobra";
    let last_name = "Cávataio Fêrry";
    let email = "thecobra@cavataio.com";
    data["auth_token"] = auth_token;
    data["first_name"] = first_name;
    data["last_name"] = last_name;
    data["email"] = email;
    data["phone"] = "";

    axios.post("/api/actions/create_client.php", qs.stringify(data))
      .then(response => {
        assert.equal(response.data.message, "Client created successfully");
        done();
      })
      .catch(err => done(err))
  });

  test("user enters valid user w/ empty phone creates record in db", function (done) {
    let first_name = "Mîchélè Cobra";
    let last_name = "Cávataio Fêrry";
    let email = "thecobra@cavataio.com";
    data["auth_token"] = auth_token;
    data["first_name"] = first_name;
    data["last_name"] = last_name;
    data["email"] = email;
    data["phone"] = "";

    axios.post("/api/actions/create_client.php", qs.stringify(data))
      .then(response => {
        return conn.query(`
      SELECT *
      FROM Usuario
      WHERE nombre = '${first_name}'
      AND apellido = '${last_name}'
      AND email = '${email}';
      `);
      })
      .then(dbres => {
        assert.lengthOf(dbres.rows, 1)
        done();
      })
      .catch(err => done(err))
  });

  test("user enters valid user creates record in db of type PROSPECTO(1)", function (done) {
    let first_name = "Mîchélè Cobra";
    let last_name = "Cávataio Fêrry";
    let email = "thecobra@cavataio.com";
    data["auth_token"] = auth_token;
    data["first_name"] = first_name;
    data["last_name"] = last_name;
    data["email"] = email;

    axios.post("/api/actions/create_client.php", qs.stringify(data))
      .then(response => {
        return conn.query(`
      SELECT *
      FROM Usuario
      WHERE nombre = '${first_name}'
      AND apellido = '${last_name}'
      AND email = '${email}';
      `);
      })
      .then(dbres => {
        assert.equal(dbres.rows[0].tipo, 1);
        done();
      })
      .catch(err => done(err))
  });

  /* F: Clean phone */
  test("user can enter phone however and it will be cleaned", function (done) {
    let first_name = "Mîchélè Cobra";
    let last_name = "Cávataio Fêrry";
    let email = "thecobra@cavataio.com";
    let phone = "+52 (442) 124-3571";
    data["auth_token"] = auth_token;
    data["first_name"] = first_name;
    data["last_name"] = last_name;
    data["email"] = email;
    data["phone"] = phone;

    axios.post("/api/actions/create_client.php", qs.stringify(data))
      .then(response => {
        return conn.query(`
      SELECT *
      FROM Usuario
      WHERE nombre = '${first_name}'
      AND apellido = '${last_name}'
      AND email = '${email}';
      `);
      })
      .then(dbres => {
        assert.equal(dbres.rows[0].telefono, "524421243571");
        done();
      })
      .catch(err => done(err))
  });

  /* M: SQL injections */
  test("user cant perform SQL injections since it returns 400", function (done) {
    let last_name = "Cavataio";
    let email = "thecobra@cavataio.com";
    data["auth_token"] = auth_token;
    data["first_name"] = "'; DELETE FROM User WHERE email = 'crazy4coco@scarface.es'; SELECT * FROM User WHERE email = '";
    data["last_name"] = last_name;
    data["email"] = email;

    axios.post("/api/actions/create_client.php", qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeded with status code ${response.status}.`);
        done();
      })
      .catch(axiosError => {
        if (axiosError.response) {
          let response = axiosError.response;
          assert.equal(response.status, 400);
          done();
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });

  test("user cant perform SQL injections since it doesnt affect the db", function (done) {
    let last_name = "Cavataio";
    let email = "thecobra@cavataio.com";
    data["auth_token"] = auth_token;
    data["first_name"] = "'; DELETE FROM Usuario WHERE email = 'crazy4coco@scarface.es'; SELECT * FROM User WHERE email = '";
    data["last_name"] = last_name;
    data["email"] = email;

    axios.post("/api/actions/create_client.php", qs.stringify(data))
      .then(response => {
        assert.fail(`Request succeded with status code ${response.status}.`);
        done();
      })
      .catch(axiosError => {
        if (axiosError.response) {
          return conn.query(`
        SELECT * FROM Usuario WHERE email = 'crazy4coco@scarface.es';
        `)
            .then(dbres => {
              assert.lengthOf(dbres.rows, 1);
              done();
            });
        } else {
          done(axiosError);
        }
      })
      .catch(err => done(err));
  });

  teardown(done => {
    conn.query(`
      DELETE FROM Usuario WHERE email = 'thecobra@cavataio.com';
    `).then(() => done()).catch(err => done(err));
  });

  suiteTeardown(function (done) {
    // We delete the test user
    conn.query(`
      DELETE FROM Usuario WHERE email = 'crazy4coco@scarface.es';
      DELETE FROM Usuario WHERE email = 'pewdieliza@youtube.com';
      DELETE FROM Rol WHERE nombre = 'test_role';
      DELETE FROM Rol WHERE nombre = 'alien';
    `).then(() => {
      // We close the connection to the database
      return conn.end();
    }).then(() => done()).catch(err => done(err));
  });
});
