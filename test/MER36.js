const { assert } = require("chai"); // We will be using Chai's assert library. https://www.chaijs.com/guide/styles/#assert
const conn = require("./utils/db.js").conn(); // We will also be relying on some utils functions for database connections.
const axios = require("axios"); // We will use Axios for asynchronous calls. https://www.npmjs.com/package/axios
const dbadduser = require("./utils/db.js").adduser;
axios.defaults.baseURL = "http://localhost:8080";

const shajs = require("sha.js") // This is an encryption library we will be using to store the passwords

const qs = require("qs");

suite("MER36 - Eliminar Producto", function(){
    var data, auth_token, alt_auth_token, user_id;
  
    suiteSetup(function(done){
  
        /* We start the connection to the database and since this is 
         * an async request we use await, to wait for it to finish. */
      conn.connect().then(() => {
        return dbadduser(
          conn, {
              nombre: "Ben",
              apellido: "Dove",
              email: "whosyourcookie@gmail.com",
              tipo: 2,
              contrasena: shajs("sha256").update("123").digest("hex")
            }, [ 
              "ADD_PRODUCT",
              "DELETE_PRODUCT",
            ]
        );
      }).then(dbres => {
        user_id = dbres.rows[0].id_usuario;
        return dbadduser(
          conn, {
              nombre: "Felix",
              apellido: "Koshy",
              email: "pewdieliza@youtube.com",
              tipo: 0,
              contrasena: shajs("sha256").update("say_klk").digest("hex")
          }, [
            "GET_CLIENTS"
        ], "alien");
      }).then(() => {
        return axios.post("/api/actions/login.php", qs.stringify({
          email: 'whosyourcookie@gmail.com',
          password: '123'
        }));
      }).then(response => {
        auth_token = response.data.data.auth_token;
        return axios.post("/api/actions/login.php", qs.stringify({
          email: 'pewdieliza@youtube.com',
          password: 'say_klk'
        }));
      }).then(response => {
        alt_auth_token = response.data.data.auth_token;
        return conn.query(`
          INSERT INTO tipoproducto (id, nombre) VALUES (888888, 'Test MER36');
        `)
      }).then(() => {
        return conn.query(`
        INSERT INTO 
        producto (id, nombre, precio, cantidad, estado, tipo, talla, sexo, descripcion)
        VALUES (
            101010,
           'Producto testing MER36',
            300.00,
            50,
            1,
            888888,
           'M',
           'U',
           'Ejemplo de descripción'
        );
      `);
      }).then(() => {done()}).catch(err => {done(err)}); 
    
    });

    setup(function(){
        // We create the dummy data for our new client
        data = {};
    });

    test("user without authentication returns 401", function(done){
        // We make a POST request with an empty request body.
        axios.post(`/api/actions/delete_product.php`, data)
          // If the request is successfully sent then...
          .then(function(response){
            //It means that the status code is in the 1XX-3XX range, thus we must fail
            assert.fail(`Request succeded with status code ${response.status}.`);
    
            // We tell Mocha we are done.
            done();
    
            // If the request had an error we catch it and...
          }).catch((axiosErr) => {
            // If we got a response...
            if(axiosErr.response){
              // We access the response object in the error
              let response = axiosErr.response;
    
              // We compare the actual response code to our expected 401
              assert.equal(response.status, 401);
    
              // We tell Mocha we are done.
              done();
            } else {
              // ...else report it to Mocha
              done(axiosErr);
            }
          }).catch((assertErr) => {
            // We notify Mocha of any other error.
            done(assertErr);
        });
    });

    test("user without authentication returns correct message", function(done){
        axios.post(`/api/actions/delete_product.php`, data)
          .then(function(response){
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        }).catch((axiosErr) => {
            if(axiosErr.response){
              let response = axiosErr.response;
              assert.equal(response.data.message, "Not authenticated");
              done();
            } else {
              done(axiosErr);
            }
        }).catch((assertErr) => {
            done(assertErr);
        });
    });

    /* M: The user is does not have permissions */
    test("user doesn't have permissions should return 403", function(done){
        data["auth_token"] =  alt_auth_token;
        axios.post(`/api/actions/delete_product.php`, qs.stringify(data))
        .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
        .catch(axiosError => {
            if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.status, 403);
            done();
            } else {
            done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test("user doesn't have permissions should return correct message", function(done){
        data["auth_token"] =  alt_auth_token;
        axios.post(`/api/actions/delete_product.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosError => {
          if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.data.message, "Permission denied");
            done();
          } else {
            done(axiosError);
          }
        })
        .catch(error => done(error));
    });

    test("authenticated user enters invalid id returns 400", function(done) {
        data["auth_token"] =  auth_token;
        data["id_product"] =  'AAAA';
        axios.post(`/api/actions/delete_product.php`, qs.stringify(data))
        .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
        .catch(axiosError => {
            if(axiosError.response){
              let response = axiosError.response;
              assert.equal(response.status, 400);
              done();
            } else {
              done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`user enters invalid product id returns correct message`, function(done) {
        data["auth_token"] =  auth_token;
        data["id_product"] =  'AAAA';
        axios.post(`/api/actions/delete_product.php`, qs.stringify(data))
          .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
          .catch(axiosError => {
            if(axiosError.response){
              let response = axiosError.response;
              assert.equal(response.data.message.errors['id_product'], `Invalid id_product`);
              done();
            } else {
              done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`user enters valid values returns 201`, function(done) {
        data["auth_token"] =  auth_token;
        data["id_product"] =  '101010';
        axios.post(`/api/actions/delete_product.php`, qs.stringify(data))
          .then(response => {
            assert.equal(response.status, 200);
            return conn.query(`
                INSERT INTO 
                producto (id, nombre, precio, cantidad, estado, tipo, talla, sexo, descripcion)
                VALUES (
                    101010,
                    'Producto testing MER36',
                    300.00,
                    50,
                    1,
                    888888,
                    'M',
                    'U',
                    'Ejemplo de descripción'
                );
            `);
            
        })
        .then(cleanup => {
            done();
        })
        .catch(err => done(err));
    });

    test(`user enters valid values deletes record in db`, function(done) {
        data["auth_token"] =  auth_token;
        data["id_product"] =  101010;
        axios.post(`/api/actions/delete_product.php`, qs.stringify(data))
        .then(response => {
            return conn.query(`
                SELECT *
                FROM producto
                WHERE id = 101010;
            `)
        })
        .then(dbres => {
            assert.lengthOf(dbres.rows, 0);
            return conn.query(`
                INSERT INTO 
                producto (id, nombre, precio, cantidad, estado, tipo, talla, sexo, descripcion)
                VALUES (
                    101010,
                    'Producto testing MER36',
                    300.00,
                    50,
                    1,
                    888888,
                    'M',
                    'U',
                    'Ejemplo de descripción'
                );
            `);
        })
        .then(cleanup => {
            done();
        })
        .catch(err => done(err))
    });

    suiteTeardown(function(done){
        conn.query(`
          DELETE FROM producto WHERE id = 101010
        `).then(() => {
          return conn.query(`
            DELETE FROM usuario WHERE id = ${user_id};
            DELETE FROM usuario WHERE email LIKE '%pewdieliza%';
            DELETE FROM rol WHERE nombre = 'test_role';
            DELETE FROM rol WHERE nombre = 'alien';
          `)
        }).then(() => {
          return conn.query(`
            DELETE FROM tipoproducto WHERE id = 888888;
          `)
        }).then(() => {
            // We close the connection to the database
            conn.end();
        }).then(() => done()).catch(err => done(err));
    });

});
