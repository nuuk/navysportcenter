/* CLI4 - Consultar clientes */
const { assert } = require("chai");
const conn = require("./utils/db.js").conn();
const dbadduser = require("./utils/db.js").adduser;
const axios = require("axios");
axios.defaults.baseURL = "http://localhost:8080";

const shajs = require("sha.js");
const qs = require("qs");


suite("CLI4 - Consultar clientes", function(){
  var data, auth_token;
  var clients = [{
    id: '0',
    nombre: "Christopher",
    apellido: "Coke",
    email: "dudus@coke.co",
    telefono: null,
    tipo: '1',
    estado: '1'
  }, {
    id: '0',
    nombre: "Carmine",
    apellido: "Galante",
    email: "thecigar@galante.it",
    telefono: null,
    tipo: '2',
    estado: '1'
  }];

  suiteSetup(done => {
    conn.connect().then(() => {
      return dbadduser(
        conn, {
            nombre: clients[0].nombre,
            apellido: clients[0].apellido,
            email: clients[0].email,
            tipo: clients[0].tipo,
            contrasena: shajs("sha256").update("say_hello").digest("hex")
          }, [ 
            "GET_CLIENTS"
          ]
      );
    }).then(dbres => {
      clients[0].id = ""+dbres.rows[0].id_usuario;
      clients[0].edit = `/clientes/${dbres.rows[0].id_usuario}/edit/`;
      clients[0].delete = `/api/actions/delete_client.php?id=${dbres.rows[0].id_usuario}`;
      return dbadduser(
        conn, {
            nombre: clients[1].nombre,
            apellido: clients[1].apellido,
            email: clients[1].email,
            tipo: clients[1].tipo,
            contrasena: shajs("sha256").update("say_hello").digest("hex")
        }, [
          "EDIT_EMPLOYEE"
        ],
        "alien"
      );
    }).then(dbres => {
      clients[1].id = ""+dbres.rows[0].id_usuario;
      clients[1].edit = `/clientes/${dbres.rows[0].id_usuario}/edit/`;
      clients[1].delete = `/api/actions/delete_client.php?id=${dbres.rows[0].id_usuario}`;
      return dbadduser(
        conn, {
            nombre: "Felix",
            apellido: "Koshy",
            email: "pewdieliza@youtube.com",
            tipo: 0,
            contrasena: shajs("sha256").update("say_klk").digest("hex")
        }, [
          "ADD_EMPLOYEE"
        ],
        "alien"
      );
    }).then(() => {
      return axios.post("/api/actions/login.php", qs.stringify({
        email: clients[0].email,
        password: 'say_hello'
      }));
    }).then(response => {
      auth_token = response.data.data.auth_token;
      return axios.post("/api/actions/login.php", qs.stringify({
        email: 'pewdieliza@youtube.com',
        password: 'say_klk'
      }));
    }).then(response => {
      alt_auth_token = response.data.data.auth_token;
      done();
    }).catch(err => done(err));
  });

  setup(function(){
    data = {};
  });

  test("user without authentication returns 401", done => {
    axios.post(`/api/actions/get_clients.php`)
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.status, 401);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user without authentication returns correct message", done => {
    axios.post(`/api/actions/get_clients.php`)
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.data.message, `Not authenticated`);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user doesn't have permissions returns 403", done => {
    data["auth_token"] = alt_auth_token;
    axios.post(`/api/actions/get_clients.php`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.status, 403);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user doesn't have permissions returns correct message", done => {
    data["auth_token"] = alt_auth_token;
    axios.post(`/api/actions/get_clients.php`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.data.message, `Permission denied`);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user succesfully retrieves information returns 200", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php`, qs.stringify(data))
    .then(response => {
      assert.equal(response.status, 200);
      done();
    })
    .catch(err => done(err));
  });

  test("user succesfully retrieves information returns correct message", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php`, qs.stringify(data))
    .then(response => {
      assert.equal(response.data.message, `Clients retrieved successfully`);
      done();
    })
    .catch(err => done(err));
  });

  test("user succesfully retrieves information of certain users", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php`, qs.stringify(data))
    .then(response => {
      assert.includeDeepMembers(response.data.data, clients);
      done();
    })
    .catch(err => done(err));
  });

  test("user succesfully retrieves only clients", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php`, qs.stringify(data))
    .then(response => {
      response.data.data.forEach(itm => {
        assert.isAtLeast(parseInt(itm.tipo), 1);
      });
      done();
    })
    .catch(err => done(err));
  });

  test("user succesfully retrieves specific client returns 200", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php?id=${clients[0].id}`, qs.stringify(data))
    .then(response => {
      assert.equal(response.status, 200);
      done();
    })
    .catch(err => done(err));
  });

  test("user succesfully retrieves specific client returns correct message", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php?id=${clients[0].id}`, qs.stringify(data))
    .then(response => {
      assert.equal(response.data.message, `Clients retrieved successfully`);
      done();
    })
    .catch(err => done(err));
  });

  test("user succesfully retrieves exactly one client", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php?id=${clients[0].id}`, qs.stringify(data))
    .then(response => {
      assert.lengthOf(response.data.data, 1);
      done();
    })
    .catch(err => done(err));
  });

  test("user succesfully retrieves specific client returns expected client", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php?id=${clients[0].id}`, qs.stringify(data))
    .then(response => {
      conn.query(`
        SELECT * FROM Usuario WHERE id = ${clients[0].id};
      `).then(dbres => {
        let dbdata = dbres.rows[0];
        let apidata = response.data.data[0];
        assert.equal(dbdata.id, apidata.id);
        assert.equal(dbdata.nombre, apidata.nombre);
        assert.equal(dbdata.apellido, apidata.apellido);
        assert.equal(dbdata.email, apidata.email);
        assert.equal(dbdata.tipo, apidata.tipo);
        assert.equal(dbdata.telefono, apidata.telefono);
        done();
      })
      .catch(err => done(err));
    })
    .catch(err => done(err));
  });

  test("user attempts not found id returns 404", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php?id=102496`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.status, 404);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user attempts not found id returns correct message", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php?id=102496`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.data.message, `Client not found.`);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user attempts not found id returns no data", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php?id=102496`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.isUndefined(response.data.data);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user attempts invalid id returns 400", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php?id=klkwawawa`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.status, 400);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user attempts invalid id returns correct message", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php?id=_909_`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.data.message.errors.id, "Invalid id");
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user attempts SQLINJECTION returns 400", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php?id=;DELETE FROM Usuario;SELECT FROM Usuario WHERE `, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.status, 400);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user attempts SQLINJECTION returns correct message", done => {
    data["auth_token"] = auth_token;
    axios.post(`/api/actions/get_clients.php?id=;DELETE FROM Usuario;SELECT FROM Usuario WHERE `, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.data.message.errors.id, "Invalid id");
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  suiteTeardown(done => {
    conn.query(`
      DELETE FROM
      Usuario WHERE 
      email = '${clients[0].email}' OR
      email = '${clients[1].email}' OR
      email = 'pewdieliza@youtube.com';
      DELETE FROM Rol WHERE nombre = 'test_role';
      DELETE FROM Rol WHERE nombre = 'alien';
   `)
    .then(dbres => {return conn.end()})
    .then(dbres => done())
    .catch(err => done(err));
  });
});
