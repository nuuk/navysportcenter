/* EMP12.js - Consultar Empleados */
const { assert } = require("chai");
const conn = require("./utils/db.js").conn();
const dbadduser = require("./utils/db.js").adduser;
const axios = require("axios");
axios.defaults.baseURL = "http://localhost:8080";

const shajs = require("sha.js");
const qs = require("qs");

suite("EMP12.js - Consultar Empleados", function(){
  var data, auth_token, alt_auth_token;
  var employees = [{
    id: '0',
    nombre: "Christopher",
    apellido: "Coke",
    email: "dudus@coke.co",
    telefono: null,
    estado: '1',
    tipo: '0',
  }, {
    id: '0',
    nombre: "Carmine",
    apellido: "Galante",
    email: "thecigar@galante.it",
    telefono: null,
    estado: '1',
    tipo: '0',
  }];

  suiteSetup(done => {
    conn.connect().then(() => {
      return dbadduser(
        conn, {
            nombre: employees[0].nombre,
            apellido: employees[0].apellido,
            email: employees[0].email,
            tipo: employees[0].tipo,
            contrasena: shajs("sha256").update("say_hello").digest("hex")
          }, [ 
            "GET_EMPLOYEES"
          ]
      );
    }).then(dbres => {
      employees[0].id = ""+dbres.rows[0].id_usuario;
      employees[0].edit = `/empleados/${dbres.rows[0].id_usuario}/edit/`;
      employees[0].delete = `/api/actions/delete_employee.php?id=${dbres.rows[0].id_usuario}`;
      return dbadduser(
        conn, {
            nombre: employees[1].nombre,
            apellido: employees[1].apellido,
            email: employees[1].email,
            tipo: employees[1].tipo,
            contrasena: shajs("sha256").update("say_hello").digest("hex")
        }, [
          "EDIT_EMPLOYEE"
        ],
        "alien"
      );
    }).then(dbres => {
      employees[1].id = ""+dbres.rows[0].id_usuario;
      employees[1].edit = `/empleados/${dbres.rows[0].id_usuario}/edit/`;
      employees[1].delete = `/api/actions/delete_employee.php?id=${dbres.rows[0].id_usuario}`;
      return dbadduser(
        conn, {
            nombre: "Felix",
            apellido: "Koshy",
            email: "pewdieliza@youtube.com",
            tipo: 2,
            contrasena: shajs("sha256").update("say_klk").digest("hex")
        }, [
          "ADD_EMPLOYEE"
        ],
        "alien"
      );
    }).then(() => {
      return axios.post("/api/actions/login.php", qs.stringify({
        email: employees[0].email,
        password: 'say_hello'
      }));
    }).then(response => {
      auth_token = response.data.data.auth_token;
      return axios.post("/api/actions/login.php", qs.stringify({
        email: 'pewdieliza@youtube.com',
        password: 'say_klk'
      }));
    }).then(response => {
      alt_auth_token = response.data.data.auth_token;
      done();
    }).catch(err => done(err));
  });

  setup(function(){
    data = {};
  });

  test("user without authentication returns 401", done => {
    axios.post(`/api/actions/get_employees.php`)
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.status, 401);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user without authentication returns correct message", done => {
    axios.post(`/api/actions/get_employees.php`)
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.data.message, `Not authenticated`);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user doesn't have permissions returns 403", done => {
    data["auth_token"] =  alt_auth_token;
    axios.post(`/api/actions/get_employees.php`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.status, 403);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user doesn't have permissions returns correct message", done => {
    data["auth_token"] =  alt_auth_token;
    axios.post(`/api/actions/get_employees.php`, qs.stringify(data))
    .then(response => {
      assert.fail(`Request succeded with status code ${response.status}.`);
      done();
    })
    .catch(axiosErr => {
      if(axiosErr.response){
        let response = axiosErr.response;
        assert.equal(response.data.message, `Permission denied`);
        done();
      } else {
        done(axiosErr);
      }
    })
    .catch(err => done(err));
  });

  test("user successfully retrieves information returns 200", done => {
    data["auth_token"] =  auth_token;
    axios.post(`/api/actions/get_employees.php`, qs.stringify(data))
    .then(response => {
      assert.equal(response.status, 200);
      done();
    })
    .catch(err => done(err));
  });

  test("user successfully retrieves information returns correct message", done => {
    data["auth_token"] =  auth_token;
    axios.post(`/api/actions/get_employees.php`, qs.stringify(data))
    .then(response => {
      assert.equal(response.data.message, `Employees retrieved successfully`);
      done();
    })
    .catch(err => done(err));
  });

  test("user successfully retrieves information of certain users", done => {
    data["auth_token"] =  auth_token;
    axios.post(`/api/actions/get_employees.php`, qs.stringify(data))
    .then(response => {
      assert.includeDeepMembers(response.data.data, employees);
      done();
    })
    .catch(err => done(err));
  });

  test("user successfully retrieves only employees", done => {
    data["auth_token"] =  auth_token;
    axios.post(`/api/actions/get_employees.php`, qs.stringify(data))
    .then(response => {
      response.data.data.forEach(itm => {
        assert.equal(parseInt(itm.tipo), 0);
      });
      done();
    })
    .catch(err => done(err));
  });

  suiteTeardown(done => {
    conn.query(`
      DELETE FROM
      Usuario WHERE 
      email = '${employees[0].email}' OR
      email = '${employees[1].email}' OR
      email = 'pewdieliza@youtube.com';
      DELETE FROM Rol WHERE nombre = 'test_role';
      DELETE FROM Rol WHERE nombre = 'alien';
   `)
    .then(dbres => {return conn.end()})
    .then(dbres => done())
    .catch(err => done(err));
  });
});
