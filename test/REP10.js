const { assert } = require("chai");
const conn = require("./utils/db.js").conn();
const dbadduser = require("./utils/db.js").adduser;
const axios = require("axios");
axios.defaults.baseURL = "http://localhost:8080";

const shajs = require("sha.js");
const qs = require("qs");

suite("REP10 - Consultar reporte de asistencia por periodos", function(){
    var data, auth_token, client_id;

    suiteSetup(function(done){
      conn.connect().then(() => {
        return dbadduser(
          conn, {
              nombre: "Ben",
              apellido: "Dove",
              email: "whosyourcookie@gmail.com",
              tipo: 2,
              contrasena: shajs("sha256").update("123").digest("hex")
            }, [ 
              "GET_ATTENDANCES",
            ]
        );
      }).then(dbres => {
        client_id = dbres.rows[0].id_usuario;
        return dbadduser(
          conn, {
              nombre: "Felix",
              apellido: "Koshy",
              email: "pewdieliza@youtube.com",
              tipo: 0,
              contrasena: shajs("sha256").update("say_klk").digest("hex")
          }, [
            "GET_CLIENTS"
        ], "alien");
      }).then(() => {
        return axios.post("/api/actions/login.php", qs.stringify({
          email: 'whosyourcookie@gmail.com',
          password: '123'
        }));
      }).then(response => {
        auth_token = response.data.data.auth_token;
        return axios.post("/api/actions/login.php", qs.stringify({
          email: 'pewdieliza@youtube.com',
          password: 'say_klk'
        }));
      }).then(response => {
          alt_auth_token = response.data.data.auth_token;
          return conn.query(`
              INSERT INTO
              asistencia (fecha, tiempo, cliente, estado) 
              VALUES ('2020-01-01', '12:00:00', ${client_id}, 1)
          `)
        }).then(() => done()).catch(err => {done(err)});
    });

    setup(function(){
        // We create the dummy data for our new client
        data = {};
    });

    test("user without authentication returns 401", function(done){
        // We make a POST request with an empty request body.
        axios.post(`/api/actions/get_attendance_history.php`, data)
          // If the request is successfully sent then...
          .then(function(response){
            //It means that the status code is in the 1XX-3XX range, thus we must fail
            assert.fail(`Request succeded with status code ${response.status}.`);
    
            // We tell Mocha we are done.
            done();
    
            // If the request had an error we catch it and...
          }).catch((axiosErr) => {
            // If we got a response...
            if(axiosErr.response){
              // We access the response object in the error
              let response = axiosErr.response;
    
              // We compare the actual response code to our expected 401
              assert.equal(response.status, 401);
    
              // We tell Mocha we are done.
              done();
            } else {
              // ...else report it to Mocha
              done(axiosErr);
            }
          }).catch((assertErr) => {
            // We notify Mocha of any other error.
            done(assertErr);
        });
    });

    test("user without authentication returns correct message", function(done){
        axios.post(`/api/actions/get_attendance_history.php`, data)
          .then(function(response){
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        }).catch((axiosErr) => {
            if(axiosErr.response){
              let response = axiosErr.response;
              assert.equal(response.data.message, "Not authenticated");
              done();
            } else {
              done(axiosErr);
            }
        }).catch((assertErr) => {
            done(assertErr);
        });
    });

    /* M: The user is does not have permissions */
    test("user doesn't have permissions should return 403", function(done){
        data["auth_token"] = alt_auth_token;
        axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
        .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
        .catch(axiosError => {
            if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.status, 403);
            done();
            } else {
            done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test("user doesn't have permissions should return correct message", function(done){
        data["auth_token"] = alt_auth_token;
        axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
        .then(response => {
          assert.fail(`Request succeded with status code ${response.status}.`);
          done();
        })
        .catch(axiosError => {
          if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.data.message, "Permission denied");
            done();
          } else {
            done(axiosError);
          }
        })
        .catch(error => done(error));
    });

    // M: The user enters an invalid lower date
    // M: The user enters an invalid upper date
    // M: The user enters an invalid lower time
    // M: The user enters an invalid upper time
    var fields_to_validate = [
        {name: "lower_date", value: "2019-01-001"},
        {name: "upper_date", value: "2019-14-01"},
        {name: "lower_time", value: "40:55:00"},
        {name: "upper_time", value: "10:75:00"}
    ];

    fields_to_validate.forEach((field) => {
        test(`user doesn't enter a valid ${field.name} returns 400`, function(done){
            data["auth_token"] = auth_token;
            data[field.name] = field.value
            axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
            .then(response => {
              assert.fail(`Request succeded with status code ${response.status}.`);
              done();
            })
            .catch(axiosError => {
              if(axiosError.response){
                let response = axiosError.response;
                assert.equal(response.status, 400);
                done();
              } else {
                done(axiosError);
              }
            })
            .catch(error => done(error));
        });

        test(`user doesn't enter a valid ${field.name} returns the correct message`, function(done){
            data["auth_token"] = auth_token;
            data[field.name] = field.value;
            axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
            .then(response => {
              assert.fail(`Request succeded with status code ${response.status}.`);
              done();
            })
            .catch(axiosError => {
              if(axiosError.response){
                let response = axiosError.response;
                assert.equal(response.data.message.errors[field.name], `Invalid ${field.name}`);
                done();
              } else {
                done(axiosError);
              }
            })
            .catch(error => done(error));
          });
    });

    test(`authenticated user enters invalid lower date returns 400`, function(done) {
        data["auth_token"] = auth_token;
        data["lower_date"] = '201-06-01';
        data["upper_date"] = '2020-01-01';
        data["lower_time"] = '11:00:00';
        data["upper_time"] = '14:00:00';
        axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
          .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
          .catch(axiosError => {
            if(axiosError.response){
              let response = axiosError.response;
              assert.equal(response.status, 400);
              done();
            } else {
              done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`authenticated user enters invalid lower date returns correct message`, function(done) {
        data["auth_token"] = auth_token;
        data["lower_date"] = '20100-06-01';
        data["upper_date"] = '2020-01-01';
        data["lower_time"] = '11:00:00';
        data["upper_time"] = '14:00:00';
        axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
        .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
        .catch(axiosError => {
            if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.data.message.errors['lower_date'], `Invalid lower_date`);
            done();
            } else {
            done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`authenticated user enters invalid upper date returns 400`, function(done) {
        data["auth_token"] = auth_token;
        data["lower_date"] = '2019-06-01';
        data["upper_date"] = '202-01-01';
        data["lower_time"] = '11:00:00';
        data["upper_time"] = '14:00:00';
        axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
          .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
          .catch(axiosError => {
            if(axiosError.response){
              let response = axiosError.response;
              assert.equal(response.status, 400);
              done();
            } else {
              done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`authenticated user enters invalid upper date returns correct message`, function(done) {
        data["auth_token"] = auth_token;
        data["lower_date"] = '2019-06-01';
        data["upper_date"] = '202-01-01';
        data["lower_time"] = '11:00:00';
        data["upper_time"] = '14:00:00';
        axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
        .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
        .catch(axiosError => {
            if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.data.message.errors['upper_date'], `Invalid upper_date`);
            done();
            } else {
            done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`authenticated user enters invalid lower time returns 400`, function(done) {
        data["auth_token"] = auth_token;
        data["lower_date"] = '2019-06-01';
        data["upper_date"] = '2020-01-01';
        data["lower_time"] = '31:00:00';
        data["upper_time"] = '14:00:00';
        axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
          .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
          .catch(axiosError => {
            if(axiosError.response){
              let response = axiosError.response;
              assert.equal(response.status, 400);
              done();
            } else {
              done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`authenticated user enters invalid lower time returns correct message`, function(done) {
        data["auth_token"] = auth_token;
        data["lower_date"] = '2019-06-01';
        data["upper_date"] = '2020-01-01';
        data["lower_time"] = '31:00:00';
        data["upper_time"] = '14:00:00';
        axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
        .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
        .catch(axiosError => {
            if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.data.message.errors['lower_time'], `Invalid lower_time`);
            done();
            } else {
            done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`authenticated user enters invalid upper time returns 400`, function(done) {
        data["auth_token"] = auth_token;
        data["lower_date"] = '2019-06-01';
        data["upper_date"] = '2020-01-01';
        data["lower_time"] = '6:00:00';
        data["upper_time"] = '23:70:00';
        axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
          .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
          .catch(axiosError => {
            if(axiosError.response){
              let response = axiosError.response;
              assert.equal(response.status, 400);
              done();
            } else {
              done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`authenticated user enters invalid upper time returns correct message`, function(done) {
        data["auth_token"] = auth_token;
        data["lower_date"] = '2019-06-01';
        data["upper_date"] = '2020-01-01';
        data["lower_time"] = '6:00:00';
        data["upper_time"] = '23:70:00';
        axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
        .then(response => {
            assert.fail(`Request succeded with status code ${response.status}.`);
            done();
        })
        .catch(axiosError => {
            if(axiosError.response){
            let response = axiosError.response;
            assert.equal(response.data.message.errors['upper_time'], `Invalid upper_time`);
            done();
            } else {
            done(axiosError);
            }
        })
        .catch(error => done(error));
    });

    test(`authenticated user enters valid date and time values returns 200`, function(done) {
        data["auth_token"] = auth_token;
        data["lower_date"] = '2019-06-01';
        data["upper_date"] = '2020-01-01';
        data["lower_time"] = '6:00:00';
        data["upper_time"] = '23:00:00';
        axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
        .then(response => {
            assert.equal(response.status, 200);
        })
        .then(cleanup => {
            done();
        })
        .catch(err => done(err));
    });

    test(`authenticated user enters valid date and time values returns records from the db`, function(done) {
        data["auth_token"] = auth_token;
        data["lower_date"] = '2019-06-01';
        data["upper_date"] = '2020-01-01';
        data["lower_time"] = '6:00:00';
        data["upper_time"] = '23:00:00';
        axios.post(`/api/actions/get_attendance_history.php`, qs.stringify(data))
        .then(dbres => {
            assert.isNotEmpty(dbres);
        })
        .then(cleanup => {
            done();
        })
        .catch(err => done(err));
    });

    suiteTeardown(function(done){
        // We delete the test attendance entry and user
        conn.query(`
            DELETE FROM asistencia WHERE cliente = ${client_id}
        `).then(() => {
          return conn.query(`
              DELETE FROM usuario WHERE id = ${client_id};
              DELETE FROM usuario WHERE email LIKE '%pewdie%';
              DELETE FROM Rol WHERE nombre = 'test_role';
              DELETE FROM Rol WHERE nombre = 'alien';
          `);
        }).then(() => {
            // We close the connection to the database
            return conn.end();
        }).then(() => done()).catch(err => done(err));
    });
})
