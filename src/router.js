import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)
export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/dashboard',
      name: 'dashboard',
      component: () => import('./views/Dashboard.vue')
    },{
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue'),
      beforeEnter: (to, from, next) => {
        if(sessionStorage.getItem("auth_token")){
          next("/dashboard");
        } else {
          next();
        }
      }
    },{
      path: '/membresias/new',
      name: 'create_membership',
      component: () => import('./views/CreateMembership.vue')
    },{
      path: '/editar_membresia/:id',
      name: 'EditMembership',
      component: () => import('./views/CreateMembership.vue')
    },{
      path: '/membresias',
      name: 'memberships',
      component: () => import('./views/Memberships.vue')
    }, {
      path: '/agregar_prospecto',
      name: 'add_prospect',
      component: () => import('./views/AddProspect.vue')
    }, {
      path: '/pagos_membresias/',
      name: 'memberships_payments',
      component: () => import('./views/MembershipsPayments.vue')
    }, {
      path: '/productos',
      name: 'products',
      component: () => import('./views/Products.vue')
    }, {
      path: '/crear_producto',
      name: 'create_product',
      component: () => import('./views/CreateProduct.vue')
    }, {
      path: '/pagosProductos',
      name: 'pagosProductos',
      component: () => import('./views/PagosProductos.vue')
    }, {
      path: '/esquemas_de_pago',
      name: 'PaymentSchemes',
      component: () => import('./views/PaymentSchemes.vue')
    }, {
      path: '/crear_esquema_pago',
      name: 'CreatePaymentScheme',
      component: () => import('./views/CreatePaymentScheme.vue')
    }, {
      path: '/crear_vigencia',
      name: 'CreateLife',
      component: () => import('./views/CreateLife.vue')
    }, {
      path: '/crearEmpleado',
      name: 'CreateEmployee',
      component: () => import('./views/CreateEmployee.vue')
    }, {
      path: '/editar_producto/:id',
      name: 'EditProduct',
      component: () => import('./views/CreateProduct.vue')
    }, {
      path: '/reporte_asistencias',
      name: 'attendance_history',
      component: () => import('./views/AttendanceHistory.vue')
    },
    {
      path: '/usuarios/:tipo',
      name: 'users',
      component: () => import('./views/Users.vue')
    }, {
      path: '/pasar_lista',
      name: 'workout_attendance',
      component: () => import('./views/WorkoutAttendance.vue')
    }, {
      path: '/clientes/:id/edit/',
      name: 'edit_client',
      component: () => import('./views/AddProspect.vue')
    }

  ]
});
