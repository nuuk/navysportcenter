import Vue from 'vue';
import App from './App.vue';
import BootstrapVue from 'bootstrap-vue';
import router from './router'
import VCalendar from 'v-calendar';
import * as uiv from 'uiv';

Vue.use(VCalendar,{
  formats: {
    title: 'MMMM YYYY',
    weekdays: 'W',
    navMonths: 'MMM',
    input: ['L', 'YYYY-MM-DD', 'YYYY/MM/DD'],
    dayPopover: 'L',
  }
});
Vue.use(BootstrapVue);
Vue.use(uiv);

router.beforeEach((to, from, next) => {
  if(!sessionStorage.getItem("auth_token") && to.path != "/login"){
    next("/login");
  } else {
    next();
  }
});

Vue.config.productionTip = false;
new Vue({
  router,
  render: h => h(App)
}).$mount('#app');
