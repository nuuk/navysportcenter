CREATE TABLE Usuario (
    ID SERIAL PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    apellido VARCHAR(255) NOT NULL,
    email VARCHAR(255) UNIQUE,
    telefono VARCHAR(13),
    tipo INT NOT NULL,
    estado INT NOT NULL DEFAULT 1,
    contrasena TEXT
);

CREATE TABLE Rol (
    ID SERIAL PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    descripcion TEXT,
    estado INT NOT NULL DEFAULT 1
);

CREATE TABLE Permiso (
    slug VARCHAR(255) PRIMARY KEY,
    descripcion TEXT,
    estado INT NOT NULL DEFAULT 1
);

CREATE TABLE Servicio (
    ID SERIAL PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    precio FLOAT NOT NULL,
    descripcion TEXT,
    estado INT NOT NULL DEFAULT 1
);

CREATE TABLE Membresia (
    ID SERIAL PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    precio FLOAT NOT NULL,
    descripcion TEXT,
    estado INT NOT NULL DEFAULT 1
);

CREATE TABLE Impuesto (
    ID SERIAL PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    porcentaje FLOAT NOT NULL
);

CREATE TABLE EsquemaDePago (
    ID SERIAL PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    descripcion TEXT,
    vigencia INT NOT NULL,
    porcentaje_descuento FLOAT,
    estado INT NOT NULL DEFAULT 1
);

CREATE TABLE FormaDePago (
    ID SERIAL PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    estado INT NOT NULL DEFAULT 1
);

CREATE TABLE Promocion (
    ID SERIAL PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    porcentaje_descuento FLOAT,
    estado INT NOT NULL DEFAULT 1
);

CREATE TABLE Pago (
    ID SERIAL PRIMARY KEY,
    fecha TIMESTAMP NOT NULL DEFAULT NOW(),
    subtotal_bruto FLOAT NOT NULL,
    total_descuento FLOAT,
    subtotal_neto FLOAT,
    impuesto INT REFERENCES Impuesto(ID) ON DELETE SET NULL,
    esquema_de_pago INT REFERENCES EsquemaDePago(ID) ON DELETE SET NULL,
    forma_de_pago INT REFERENCES FormaDePago(ID) ON DELETE SET NULL,
    promocion INT REFERENCES Promocion(ID) ON DELETE SET NULL,
    estado INT NOT NULL DEFAULT 1
);

CREATE TABLE TipoProducto (
    ID SERIAL PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL
);

CREATE TABLE Producto (
    ID SERIAL PRIMARY KEY,
    nombre VARCHAR(255) NOT NULL,
    tipo INT REFERENCES TipoProducto(ID) ON DELETE SET NULL,
    talla VARCHAR(255),
    precio FLOAT NOT NULL,
    cantidad INT NOT NULL,
    estado INT NOT NULL DEFAULT 1,
    sexo VARCHAR(255),
    descripcion VARCHAR(255),
    foto VARCHAR(255)
);

CREATE TABLE Asistencia (
    ID SERIAL PRIMARY KEY,
    fecha DATE NOT NULL,
    cliente INT REFERENCES Usuario(ID) ON DELETE CASCADE,
    estado INT NOT NULL DEFAULT 1,
    tiempo TIME NOT NULL
);

CREATE TABLE PagoProducto (
    ID_Pago INT REFERENCES Pago(ID) ON DELETE CASCADE,
    ID_Producto INT REFERENCES Producto(ID) ON DELETE CASCADE,
    cantidad INT NOT NULL,
    precio FLOAT NOT NULL,
    PRIMARY KEY(ID_Pago, ID_Producto)
);

CREATE TABLE Subscripcion (
    ID_Usuario INT REFERENCES Usuario(ID) ON DELETE CASCADE,
    ID_Pago INT REFERENCES Pago(ID) ON DELETE CASCADE,
    ID_Membresia INT REFERENCES Membresia(ID) ON DELETE CASCADE,
    PRIMARY KEY (ID_Usuario, ID_Pago, ID_Membresia)
);

CREATE TABLE Membresia_Servicio (
    ID_Membresia INT REFERENCES Membresia(ID) ON DELETE CASCADE,
    ID_Servicio INT REFERENCES Servicio(ID) ON DELETE CASCADE,
    cantidad INT NOT NULL,
    PRIMARY KEY (ID_Membresia, ID_Servicio)
);

CREATE TABLE Usuario_Rol (
    ID_Rol INT REFERENCES Rol(ID) ON DELETE CASCADE,
    ID_Usuario INT REFERENCES Usuario(ID) ON DELETE CASCADE,
    PRIMARY KEY (ID_Rol, ID_Usuario)
);

CREATE TABLE Rol_Permiso (
    ID_Rol INT REFERENCES Rol(ID) ON DELETE CASCADE,
    ID_Permiso VARCHAR(255) REFERENCES Permiso(slug) ON DELETE CASCADE,
    PRIMARY KEY (ID_Rol, ID_Permiso)
);

CREATE TABLE Servicio_Pago (
    ID_Servicio INT REFERENCES Servicio(ID) ON DELETE CASCADE,
    ID_Pago INT REFERENCES Pago(ID) ON DELETE CASCADE,
    precio FLOAT NOT NULL,
    PRIMARY KEY (ID_Servicio, ID_Pago)
);

CREATE TABLE Empleado_Servicio (
    ID_Servicio INT REFERENCES Servicio(ID) ON DELETE CASCADE,
    ID_Empleado INT REFERENCES Usuario(ID) ON DELETE CASCADE,
    cantidad INT NOT NULL,
    PRIMARY KEY (ID_Servicio, ID_Empleado)
);
