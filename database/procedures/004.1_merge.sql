CREATE EXTENSION pgcrypto;
CREATE OR REPLACE FUNCTION get_attendance_history(lower_date DATE, upper_date DATE, lower_time TIME, upper_time TIME)
RETURNS TABLE(
	fecha DATE,
	tiempo TIME,
	nombre VARCHAR(255),
	apellido VARCHAR(255),
	estado int
)
AS $$
BEGIN
	RETURN QUERY
	SELECT asistencia.fecha, asistencia.tiempo, usuario.nombre, usuario.apellido, asistencia.estado 
    FROM asistencia, usuario
    WHERE 
    asistencia.cliente = usuario.ID AND 
    asistencia.fecha >= lower_date AND asistencia.fecha <= upper_date AND
    asistencia.tiempo >= lower_time and asistencia.tiempo <= upper_time
    ORDER BY asistencia.fecha DESC;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION get_attendees("date" DATE, "time" TIME)
RETURNS TABLE(
	id int,
	nombre VARCHAR(255),
	apellido VARCHAR(255),
	estado int
)
AS $$
BEGIN
	RETURN QUERY
	SELECT usuario.id, usuario.nombre, usuario.apellido, asistencia.estado 
	FROM asistencia, usuario 
	WHERE asistencia.fecha = date AND 
	asistencia.tiempo = time AND
	usuario.id = asistencia.cliente;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION get_attendance(lower_limit timestamp, upper_limit timestamp)
RETURNS TABLE(
	fecha timestamp,
	nombre VARCHAR(255),
	apellido VARCHAR(255),
	estado int
)
AS $$
BEGIN
	RETURN QUERY
	SELECT asistencia.fecha, usuario.nombre, usuario.apellido, asistencia.estado 
    FROM asistencia, usuario
    WHERE 
    asistencia.cliente = usuario.ID AND 
    asistencia.fecha >= lower_date AND asistencia.fecha <= upper_date AND
    asistencia.tiempo >= lower_time and asistencia.tiempo <= upper_time
    ORDER BY asistencia.fecha DESC;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION get_attendance_top50()
RETURNS TABLE(
	fecha timestamp,
	nombre VARCHAR(255),
	apellido VARCHAR(255),
	estado int
)
AS $$
BEGIN
	RETURN QUERY
	SELECT asistencia.fecha, usuario.nombre, usuario.apellido, asistencia.estado 
    FROM asistencia, usuario
    WHERE asistencia.cliente=usuario.ID
    ORDER BY asistencia.fecha DESC
    LIMIT 50;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION get_user_permissions(user_id int)
RETURNS TABLE(
  permission VARCHAR(255)
)
AS $$
BEGIN
  RETURN QUERY
  SELECT p.slug as permission
  FROM Permiso p, Rol_Permiso r, Usuario_Rol u
  WHERE u.id_usuario = user_id
  AND r.id_rol = u.id_rol
  AND p.slug = r.id_permiso
  GROUP BY p.slug
  ORDER BY p.slug;
END;
$$ LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION get_services_list()
RETURNS TABLE (
  nom VARCHAR(255)
)
AS $$
BEGIN
    RETURN QUERY
    SELECT nombre as nom
    FROM servicio;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION add_membership(nom VARCHAR(255), descrip TEXT, prec FLOAT)
RETURNS INT
AS $$
BEGIN
    INSERT INTO membresia(nombre, precio, descripcion) VALUES (nom, prec, descrip);
    RETURN 1;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION api_get_clients()
RETURNS TABLE(
  id INT,
  nombre VARCHAR(255),
  apellido VARCHAR(255),
  email VARCHAR(255),
  tipo INT,
  telefono VARCHAR(255),
  estado INT,
  edit TEXT,
  "delete" TEXT
) AS $$
BEGIN
  RETURN QUERY
  SELECT u.id, u.nombre, u.apellido, u.email, u.tipo, u.telefono, u.estado,
  CONCAT('/clientes/',u.id,'/edit/') AS "edit",
  CONCAT('/api/actions/delete_client.php?id=',u.id) AS "delete"
  FROM Usuario u WHERE u.tipo > 0;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION api_get_client(_id INT)
RETURNS TABLE(
  id INT,
  nombre VARCHAR(255),
  apellido VARCHAR(255),
  email VARCHAR(255),
  tipo INT,
  telefono VARCHAR(255),
  edit TEXT,
  "delete" TEXT
) AS $$
BEGIN
  RETURN QUERY
  SELECT u.id, u.nombre, u.apellido, u.email, u.tipo, u.telefono,
  CONCAT('/clientes/',u.id,'/edit/') AS "edit",
  CONCAT('/api/actions/delete_client.php?id=',u.id) AS "delete"
  FROM Usuario u WHERE u.tipo > 0 AND u.id = _id;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION login(_email VARCHAR(255), _password TEXT)
RETURNS TEXT AS $$
DECLARE
  user_id INT;
  auth_token TEXT;
  auth_expiry TIMESTAMP;
BEGIN
  SELECT u.id into user_id FROM Usuario u
  WHERE u.email = _email AND u.contrasena = _password;

  SELECT token, expiry INTO auth_token, auth_expiry FROM Token t WHERE t.id = user_id;

  IF LENGTH(auth_token) > 0 AND NOW() < auth_expiry THEN  
    RETURN auth_token;
  END IF;
  IF user_id > 0 THEN
    SELECT encode(digest(CONCAT(NOW(),_email), 'sha256'), 'hex') INTO auth_token;

    INSERT INTO Token (id, token) VALUES(
      user_id, auth_token
    );

    RETURN auth_token;
  END IF;
  RETURN '';
END;
$$ LANGUAGE 'plpgsql';
CREATE OR REPLACE FUNCTION validate_token(_token TEXT) 
RETURNS TABLE(
  permissions varchar(255)
) AS $$
BEGIN
  RETURN QUERY
  SELECT id_permiso FROM Token t, Usuario_Rol ur, Rol_Permiso rp
  WHERE t.token = _token AND NOW() < t.expiry
  AND t.id = ur.id_usuario
  AND ur.id_rol = rp.id_rol;
END;
$$ LANGUAGE 'plpgsql';

CREATE OR REPLACE FUNCTION delete_product(_id INT)
RETURNS INT
AS $$
BEGIN
    DELETE FROM producto WHERE id = _id;
    RETURN 1;
END;
$$ LANGUAGE 'plpgsql';
